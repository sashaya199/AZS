﻿namespace Inventori
{
    partial class InventoriVariable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label датаLabel;
            System.Windows.Forms.Label фактические_ммLabel;
            System.Windows.Forms.Label фактические_литрыLabel;
            System.Windows.Forms.Label книжные_ммLabel;
            System.Windows.Forms.Label книжные_литрыLabel;
            System.Windows.Forms.Label расхождениеLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoriVariable));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.датаDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.расхождениеTextBox = new System.Windows.Forms.TextBox();
            this.книжные_литрыTextBox = new System.Windows.Forms.TextBox();
            this.книжные_ммTextBox = new System.Windows.Forms.TextBox();
            this.фактические_литрыTextBox = new System.Windows.Forms.TextBox();
            this.фактические_ммTextBox = new System.Windows.Forms.TextBox();
            this.СalculateEntry = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonСalculate = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonDel = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dataGridView1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.ButtonRel = new Bunifu.Framework.UI.BunifuThinButton2();
            датаLabel = new System.Windows.Forms.Label();
            фактические_ммLabel = new System.Windows.Forms.Label();
            фактические_литрыLabel = new System.Windows.Forms.Label();
            книжные_ммLabel = new System.Windows.Forms.Label();
            книжные_литрыLabel = new System.Windows.Forms.Label();
            расхождениеLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // датаLabel
            // 
            датаLabel.AutoSize = true;
            датаLabel.Location = new System.Drawing.Point(21, 117);
            датаLabel.Name = "датаLabel";
            датаLabel.Size = new System.Drawing.Size(50, 19);
            датаLabel.TabIndex = 2;
            датаLabel.Text = "Дата:";
            // 
            // фактические_ммLabel
            // 
            фактические_ммLabel.AutoSize = true;
            фактические_ммLabel.Location = new System.Drawing.Point(21, 22);
            фактические_ммLabel.Name = "фактические_ммLabel";
            фактические_ммLabel.Size = new System.Drawing.Size(134, 19);
            фактические_ммLabel.TabIndex = 6;
            фактические_ммLabel.Text = "Фактические мм:";
            // 
            // фактические_литрыLabel
            // 
            фактические_литрыLabel.AutoSize = true;
            фактические_литрыLabel.Location = new System.Drawing.Point(393, 22);
            фактические_литрыLabel.Name = "фактические_литрыLabel";
            фактические_литрыLabel.Size = new System.Drawing.Size(160, 19);
            фактические_литрыLabel.TabIndex = 8;
            фактические_литрыLabel.Text = "Фактические литры:";
            // 
            // книжные_ммLabel
            // 
            книжные_ммLabel.AutoSize = true;
            книжные_ммLabel.Location = new System.Drawing.Point(21, 70);
            книжные_ммLabel.Name = "книжные_ммLabel";
            книжные_ммLabel.Size = new System.Drawing.Size(108, 19);
            книжные_ммLabel.TabIndex = 10;
            книжные_ммLabel.Text = "Книжные мм:";
            // 
            // книжные_литрыLabel
            // 
            книжные_литрыLabel.AutoSize = true;
            книжные_литрыLabel.Location = new System.Drawing.Point(393, 70);
            книжные_литрыLabel.Name = "книжные_литрыLabel";
            книжные_литрыLabel.Size = new System.Drawing.Size(134, 19);
            книжные_литрыLabel.TabIndex = 12;
            книжные_литрыLabel.Text = "Книжные литры:";
            // 
            // расхождениеLabel
            // 
            расхождениеLabel.AutoSize = true;
            расхождениеLabel.Location = new System.Drawing.Point(393, 113);
            расхождениеLabel.Name = "расхождениеLabel";
            расхождениеLabel.Size = new System.Drawing.Size(111, 19);
            расхождениеLabel.TabIndex = 14;
            расхождениеLabel.Text = "Расхождение:";
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Выход";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(825, 546);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 1;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // датаDateTimePicker
            // 
            this.датаDateTimePicker.Location = new System.Drawing.Point(187, 113);
            this.датаDateTimePicker.Name = "датаDateTimePicker";
            this.датаDateTimePicker.Size = new System.Drawing.Size(200, 27);
            this.датаDateTimePicker.TabIndex = 3;
            this.датаDateTimePicker.Value = new System.DateTime(2018, 2, 27, 0, 0, 0, 0);
            // 
            // расхождениеTextBox
            // 
            this.расхождениеTextBox.Location = new System.Drawing.Point(559, 110);
            this.расхождениеTextBox.Name = "расхождениеTextBox";
            this.расхождениеTextBox.Size = new System.Drawing.Size(200, 27);
            this.расхождениеTextBox.TabIndex = 15;
            this.расхождениеTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Фактические_литрыTextBox_KeyPress);
            // 
            // книжные_литрыTextBox
            // 
            this.книжные_литрыTextBox.Location = new System.Drawing.Point(559, 67);
            this.книжные_литрыTextBox.Name = "книжные_литрыTextBox";
            this.книжные_литрыTextBox.Size = new System.Drawing.Size(200, 27);
            this.книжные_литрыTextBox.TabIndex = 13;
            this.книжные_литрыTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Фактические_литрыTextBox_KeyPress);
            // 
            // книжные_ммTextBox
            // 
            this.книжные_ммTextBox.Location = new System.Drawing.Point(187, 67);
            this.книжные_ммTextBox.Name = "книжные_ммTextBox";
            this.книжные_ммTextBox.Size = new System.Drawing.Size(200, 27);
            this.книжные_ммTextBox.TabIndex = 11;
            this.книжные_ммTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Фактические_литрыTextBox_KeyPress);
            // 
            // фактические_литрыTextBox
            // 
            this.фактические_литрыTextBox.Location = new System.Drawing.Point(559, 19);
            this.фактические_литрыTextBox.Name = "фактические_литрыTextBox";
            this.фактические_литрыTextBox.Size = new System.Drawing.Size(200, 27);
            this.фактические_литрыTextBox.TabIndex = 9;
            this.фактические_литрыTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Фактические_литрыTextBox_KeyPress);
            // 
            // фактические_ммTextBox
            // 
            this.фактические_ммTextBox.Location = new System.Drawing.Point(187, 19);
            this.фактические_ммTextBox.Name = "фактические_ммTextBox";
            this.фактические_ммTextBox.Size = new System.Drawing.Size(200, 27);
            this.фактические_ммTextBox.TabIndex = 7;
            this.фактические_ммTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Фактические_литрыTextBox_KeyPress);
            // 
            // СalculateEntry
            // 
            this.СalculateEntry.ActiveBorderThickness = 1;
            this.СalculateEntry.ActiveCornerRadius = 20;
            this.СalculateEntry.ActiveFillColor = System.Drawing.Color.Lime;
            this.СalculateEntry.ActiveForecolor = System.Drawing.Color.Black;
            this.СalculateEntry.ActiveLineColor = System.Drawing.Color.Lime;
            this.СalculateEntry.BackColor = System.Drawing.SystemColors.Control;
            this.СalculateEntry.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("СalculateEntry.BackgroundImage")));
            this.СalculateEntry.ButtonText = "Расчитать и записать";
            this.СalculateEntry.Cursor = System.Windows.Forms.Cursors.Hand;
            this.СalculateEntry.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.СalculateEntry.ForeColor = System.Drawing.Color.Black;
            this.СalculateEntry.IdleBorderThickness = 1;
            this.СalculateEntry.IdleCornerRadius = 20;
            this.СalculateEntry.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.СalculateEntry.IdleForecolor = System.Drawing.Color.Black;
            this.СalculateEntry.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.СalculateEntry.Location = new System.Drawing.Point(786, 52);
            this.СalculateEntry.Name = "СalculateEntry";
            this.СalculateEntry.Size = new System.Drawing.Size(104, 55);
            this.СalculateEntry.TabIndex = 1;
            this.СalculateEntry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.СalculateEntry.Click += new System.EventHandler(this.СalculateEntry_Click);
            // 
            // ButtonСalculate
            // 
            this.ButtonСalculate.ActiveBorderThickness = 1;
            this.ButtonСalculate.ActiveCornerRadius = 20;
            this.ButtonСalculate.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonСalculate.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonСalculate.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonСalculate.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonСalculate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonСalculate.BackgroundImage")));
            this.ButtonСalculate.ButtonText = "Расчитать";
            this.ButtonСalculate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonСalculate.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonСalculate.ForeColor = System.Drawing.Color.Black;
            this.ButtonСalculate.IdleBorderThickness = 1;
            this.ButtonСalculate.IdleCornerRadius = 20;
            this.ButtonСalculate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonСalculate.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonСalculate.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonСalculate.Location = new System.Drawing.Point(786, 6);
            this.ButtonСalculate.Name = "ButtonСalculate";
            this.ButtonСalculate.Size = new System.Drawing.Size(104, 40);
            this.ButtonСalculate.TabIndex = 1;
            this.ButtonСalculate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonСalculate.Click += new System.EventHandler(this.ButtonСalculate_Click);
            // 
            // ButtonDel
            // 
            this.ButtonDel.ActiveBorderThickness = 1;
            this.ButtonDel.ActiveCornerRadius = 20;
            this.ButtonDel.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonDel.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonDel.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonDel.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonDel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonDel.BackgroundImage")));
            this.ButtonDel.ButtonText = "Удалить";
            this.ButtonDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDel.ForeColor = System.Drawing.Color.Black;
            this.ButtonDel.IdleBorderThickness = 1;
            this.ButtonDel.IdleCornerRadius = 20;
            this.ButtonDel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonDel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.Location = new System.Drawing.Point(786, 113);
            this.ButtonDel.Name = "ButtonDel";
            this.ButtonDel.Size = new System.Drawing.Size(104, 40);
            this.ButtonDel.TabIndex = 1;
            this.ButtonDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.DarkGray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.DoubleBuffered = true;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.dataGridView1.HeaderForeColor = System.Drawing.Color.Black;
            this.dataGridView1.Location = new System.Drawing.Point(12, 205);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.Size = new System.Drawing.Size(917, 333);
            this.dataGridView1.TabIndex = 16;
            // 
            // ButtonRel
            // 
            this.ButtonRel.ActiveBorderThickness = 1;
            this.ButtonRel.ActiveCornerRadius = 20;
            this.ButtonRel.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonRel.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonRel.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonRel.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonRel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonRel.BackgroundImage")));
            this.ButtonRel.ButtonText = "Обновить";
            this.ButtonRel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonRel.ForeColor = System.Drawing.Color.Black;
            this.ButtonRel.IdleBorderThickness = 1;
            this.ButtonRel.IdleCornerRadius = 20;
            this.ButtonRel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonRel.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonRel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonRel.Location = new System.Drawing.Point(786, 159);
            this.ButtonRel.Name = "ButtonRel";
            this.ButtonRel.Size = new System.Drawing.Size(104, 40);
            this.ButtonRel.TabIndex = 1;
            this.ButtonRel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonRel.Click += new System.EventHandler(this.ButtonRel_Click);
            // 
            // InventoriVariable
            // 
            this.AllowEndUserDocking = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 600);
            this.CloseButton = false;
            this.CloseButtonVisible = false;
            this.ControlBox = false;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(датаLabel);
            this.Controls.Add(this.датаDateTimePicker);
            this.Controls.Add(фактические_ммLabel);
            this.Controls.Add(this.фактические_ммTextBox);
            this.Controls.Add(фактические_литрыLabel);
            this.Controls.Add(this.фактические_литрыTextBox);
            this.Controls.Add(книжные_ммLabel);
            this.Controls.Add(this.книжные_ммTextBox);
            this.Controls.Add(книжные_литрыLabel);
            this.Controls.Add(this.книжные_литрыTextBox);
            this.Controls.Add(расхождениеLabel);
            this.Controls.Add(this.расхождениеTextBox);
            this.Controls.Add(this.ButtonRel);
            this.Controls.Add(this.ButtonDel);
            this.Controls.Add(this.ButtonСalculate);
            this.Controls.Add(this.СalculateEntry);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InventoriVariable";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Document;
            this.Text = "Инвентаризация";
            this.Load += new System.EventHandler(this.InventoriVariable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private System.Windows.Forms.DateTimePicker датаDateTimePicker;
        private System.Windows.Forms.TextBox расхождениеTextBox;
        private System.Windows.Forms.TextBox книжные_литрыTextBox;
        private System.Windows.Forms.TextBox книжные_ммTextBox;
        private System.Windows.Forms.TextBox фактические_литрыTextBox;
        private System.Windows.Forms.TextBox фактические_ммTextBox;
        private Bunifu.Framework.UI.BunifuThinButton2 СalculateEntry;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonСalculate;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonDel;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView1;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonRel;
    }
}