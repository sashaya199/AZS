﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using ClassAll.Queries;
using WeifenLuo.WinFormsUI.Docking;

namespace Inventori
{
    public partial class InventoriVariable : DockContent
    {
       
        private string fuel;

        public string Fuel { get => fuel;}

        public InventoriVariable()
        {
            InitializeComponent();
        }
        private void InventoriVariable_Load(object sender, EventArgs e)
        {
            fuel = FuelValue();
            FillingTextBox();
            FillingTable();
            датаDateTimePicker.Value = DateTime.Today;
        }
        private void FillingTextBox()
        {
            InquiryMeaning renewal = new InquiryMeaning();
            int surplusLiters = renewal.RemainderFuel(Fuel); // получение остатков в литрах
            int surplusMM = renewal.RemainderFuelMM(surplusLiters.ToString(), Fuel); // получение остатков в мм 
            книжные_ммTextBox.Text = Convert.ToString(surplusMM);
            книжные_литрыTextBox.Text = Convert.ToString(surplusLiters);
        }

        private void FillingTable() // обновление данных
        {
            InquiryMeaning renewal = new InquiryMeaning();
            renewal.Data.Tables.Clear(); // удаление прошлых данных
            renewal.InventoriFilling(Fuel);// получение новых данных
            dataGridView1.DataSource = renewal.DataTable;
        }

        private string FuelValue()
        {
            InquiryMeaning renewal = new InquiryMeaning();
            string[] arryFuel = renewal.AllFuel();
            foreach (var item in arryFuel)
            {
                if (Text.Contains(item))
                {
                    return item;
                }
            }
            return "";
        }
        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void DataGridView1_Click(object sender, EventArgs e)// доделать
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            int i = dataGridView1.CurrentRow.Index;
        }

        private void ButtonСalculate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(фактические_ммTextBox.Text) && !string.IsNullOrWhiteSpace(фактические_ммTextBox.Text))
            {
                InquiryMeaning renewal = new InquiryMeaning();
                фактические_литрыTextBox.Text = Convert.ToString(renewal.RemainderFuelLiters(фактические_ммTextBox.Text, Fuel));
                расхождениеTextBox.Text = Convert.ToString(Convert.ToInt32(фактические_литрыTextBox.Text)- Convert.ToInt32(книжные_литрыTextBox.Text));
            }
            else
            {
                MessageBox.Show("Введите данные");
            }
        }

        private void СalculateEntry_Click(object sender, EventArgs e)
        {
            InquiryInto renewalInto = new InquiryInto();
            if (!string.IsNullOrEmpty(фактические_ммTextBox.Text) && !string.IsNullOrWhiteSpace(фактические_ммTextBox.Text))
            {
                ButtonСalculate_Click(sender, e); // вызов расчёта
                renewalInto.InventoriAdd(датаDateTimePicker.Value.ToString(), Fuel, фактические_ммTextBox.Text, фактические_литрыTextBox.Text, книжные_ммTextBox.Text, книжные_литрыTextBox.Text, расхождениеTextBox.Text);
            }
            else
            {
                MessageBox.Show("Введите данные");
            }
            FillingTable();// обновление datagridview 
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            string code = dataGridView1.CurrentRow.Cells["Код"].Value.ToString();
            int id = dataGridView1.CurrentRow.Index;
            dataGridView1.Rows.RemoveAt(id);
            new InquiryDeleted().DeleteInventoei(code);
        }

        private void Фактические_литрыTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void ButtonRel_Click(object sender, EventArgs e)
        {
            FillingTextBox();
            FillingTable();
            датаDateTimePicker.Value = DateTime.Today;
        }
    }
}
