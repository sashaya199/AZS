﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Policy;

namespace ClassAll.Encryption
{
    public class Cryption
    {
        public string Password(string text)
        {
            using (var sHA1 = new SHA1Managed())
            {
                byte[] hash = sHA1.ComputeHash(Encoding.UTF8.GetBytes(text));
                sHA1.Dispose();
                return Convert.ToBase64String(hash);
            }
        }
    }
}
