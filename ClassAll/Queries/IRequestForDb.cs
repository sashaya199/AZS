﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    interface IRequestForDb
    {
        void OpenBD();
        void CloseOpeBD();
        void Treatment(string commandTxt);
        void Treatment(string commandTxt, string adapterfilltabl);
        void InDB(string sqlcommand);
        string SingleValueColum(string sqlcommand, string columName);
        string[] ValueColum(string sqlcommand, string columName);
        string[] ListAllTable();
    }
}
