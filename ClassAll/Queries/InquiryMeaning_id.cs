﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    public partial class InquiryMeaning : QueriesBD
    {
        public string FuelId(string fuel)// получения id по имени
        {
            OpenBD();
            return SingleValueColum($"SELECT Fuel.* FROM Fuel where (fuel.[Вид топлива] = '{fuel}')", "Id");
        }
        public string CounterpartiesID(string counterparties)// получения id по имени
        {
            OpenBD();
            return SingleValueColum($"SELECT Counterparties.Id, Counterparties.Наименование FROM Counterparties WHERE (Counterparties.Наименование = '{counterparties}')", "Id");
        }
        public string ProviderID(string Provider)// получения id по имени
        {
            OpenBD();
            return SingleValueColum($"SELECT Provider.Id, Provider.Наименование FROM Provider WHERE(Provider.Наименование = '{Provider}'); ", "Id");
        }
    }
}
