﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClassAll.Queries
{
    public partial class InquiryMeaning : QueriesBD
    {
        public int RemainderFuel(string fuel) // остатки по выбранному виду топлива литры
        {
            OpenBD();
            int result = 0;
            string sqlcommandComing = $@"SELECT Fuel.[Вид топлива], Sum(Coming.Литры) AS Литры
                                FROM Fuel INNER JOIN Coming ON Fuel.Id = Coming.Топливо
                                GROUP BY Fuel.[Вид топлива]
                                HAVING (Fuel.[Вид топлива]= '{fuel}')";//запрос на получение прихода

            string sqlcommandConsumption = $@"SELECT Fuel.[Вид топлива], Sum(Consumption.Литры) AS Литры
                                FROM Fuel INNER JOIN Consumption ON Fuel.Id = Consumption.Топливо
                                GROUP BY Fuel.[Вид топлива]
                                HAVING (Fuel.[Вид топлива]= '{fuel}')";//запрос на получение расхода

            if (QuantityRowsRecordConsumption(fuel) > 0 && QuantityRowsRecordComing(fuel) > 0)//проверка на заполение таблиц
            {
                result =  Convert.ToInt32(SingleValueColum(sqlcommandComing, "Литры")) - Convert.ToInt32(SingleValueColum(sqlcommandConsumption, "Литры"));
            }
            else
            {
                result = Convert.ToInt32(SingleValueColum(sqlcommandComing, "Литры"));
            }
            // одним sql запросом дублируются данные
            return result;
        }
        public int RemainderFuelMM(string liters, string fuel) // нахождение мм по литрам
        {
            OpenBD();
            string sqlcommand = @"SELECT TOP 1 Миллиметры, Литры FROM " + fuel + " WHERE (Литры >= " + liters + ")";
            return Convert.ToInt32(SingleValueColum(sqlcommand, "Миллиметры"));
        }

        public int RemainderFuelLiters(string mm, string fuel) // нахождение литров по мм
        {
            OpenBD();
            string sqlcommand = $@"SELECT TOP 1 Миллиметры, Литры FROM {fuel} WHERE (Миллиметры >= {mm})";
            return Convert.ToInt32(SingleValueColum(sqlcommand, "Литры"));
        }
        public void InventoriFilling(string fuel)// заполнение инвентаризации выбранного топлива
        {
            if (fuel != null | fuel != "")
            {
                OpenBD(); // отрытие подключения
                string sqlcommand = $@"SELECT Inventori.Код, Fuel.[Вид топлива], Inventori.Дата, Inventori.[Фактические мм], Inventori.[Фактические литры], Inventori.[Книжные мм], Inventori.[Книжные литры], 
                         Inventori.Расхождение
                         FROM (Inventori INNER JOIN
                         Fuel ON Inventori.Топливо = Fuel.Id)
                         WHERE (Fuel.[Вид топлива] = '{fuel}')";
                Treatment(sqlcommand); // приминение запроса к DataTable
            }
            else
            {
                MessageBox.Show("Ошибка чтения топлива");
            }
        }
        public void ComingFilling()// заполнение учёта прихода
        {
            OpenBD();
            string sqlcommand = @"SELECT Coming.Код, Coming.Дата, Fuel.[Вид топлива] AS Топливо, Provider.Наименование AS Поставщик, Coming.Литры
                                FROM Provider INNER JOIN (Fuel INNER JOIN Coming ON Fuel.Id = Coming.Топливо) ON Provider.Id = Coming.Поставщик
                                ORDER BY Coming.Дата DESC;";
            Treatment(sqlcommand);
        }
        public void ConsumptionFilling()// заполнение учёта расхода
        {
            OpenBD();
            string sqlcommand = @"SELECT Consumption.Код, Consumption.Дата, Fuel.[Вид топлива] AS Топливо, Counterparties.Наименование AS Контрагент, Consumption.Литры
                                FROM Counterparties INNER JOIN (Fuel INNER JOIN Consumption ON Fuel.Id = Consumption.Топливо) ON Counterparties.Id = Consumption.Контрагент
                                ORDER BY Consumption.Дата DESC;";
            Treatment(sqlcommand);
        }

        public void CalibralionFuel(string fuel)
        {
            OpenBD();
            string sqcommand = $@"select {fuel}.* from {fuel} ";
            Treatment(sqcommand);
        }
    }
}
