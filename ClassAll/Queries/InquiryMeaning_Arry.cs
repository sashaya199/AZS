﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;

namespace ClassAll.Queries
{
    public partial class InquiryMeaning : QueriesBD
    {
        public string[] AllFuel() // все виды топлива
        {
            OpenBD();
            string sqlMenuSc = @"select Fuel.[Вид топлива] as Топливо from Fuel";
            string[] Fuel = ValueColum(sqlMenuSc, "Топливо");
            return Fuel;
        }
        public string[] Counterparties()// массив имен контрагентов
        {
            OpenBD();
            string sqlcommand = @"select Counterparties.Наименование from Counterparties";
            return ValueColum(sqlcommand, "Наименование");
        }
        public string[] Provider()// массив имен поставщиков
        {
            OpenBD();
            string sqlcommand = @"select Provider.Наименование from Provider";
            return ValueColum(sqlcommand, "Наименование");
        }
        public string[] TableFuel()//таблицы градуировки
        {
            OpenBD();
            string[] exclusion = { "Authorization", "Coming", "Consumption", "Counterparties", "Fuel", "Inventori", "Provider" };
            string[] allTable = ListAllTable();
            allTable = allTable.Except(exclusion).ToArray();
            return allTable;

        }
    }
}
