﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassAll.Encryption;

namespace ClassAll.Queries
{
    public class RenevalInquiry : QueriesBD
    {
        public void AddLoginAndPassword(string login, string password, string user) // Изменение логина и пароля
        {
            Cryption cryption = new Cryption();
            password = cryption.Password(password);
            OpenBD();
            string sqlcommand = $@"UPDATE [Authorization] SET [Authorization].Логин = '{login}', [Authorization].Пароль = '{password}'
                                 WHERE (([Authorization].Фамилия) = '{user}')";

            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void EditRecordComing(string id, string date, string fuel, string provider, string liters)//Изменение прихода
        {
            OpenBD();
            InquiryMeaning renewalMeaning = new InquiryMeaning();
            fuel = renewalMeaning.FuelId(fuel);
            provider = renewalMeaning.ProviderID(provider);
            string sqlcommand = $@"UPDATE Coming SET Coming.Дата = #{date}#, Coming.Топливо = {fuel}, Coming.Поставщик = {provider}, Coming.Литры = {liters} WHERE (Coming.Код= {id});";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void EditRecordConsumption(string id, string date, string fuel, string counterparties, string liters)//Изменение расхода
        {
            OpenBD();
            InquiryMeaning renewalMeaning = new InquiryMeaning();
            fuel = renewalMeaning.FuelId(fuel);
            counterparties = renewalMeaning.CounterpartiesID(counterparties);
            string sqlcommand = $@"UPDATE Consumption SET Consumption.Дата = #{date}#, Consumption.Топливо = {fuel}, Consumption.Контрагент = {counterparties}, Consumption.Литры = {liters} WHERE (Consumption.Код= {id});";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void EditCunterparties(string counterparties, string counterpartiesOld)
        {
            OpenBD();
            string sqlcommand = $@"update Counterparties set Counterparties.Наименование = {counterparties} where Counterparties.Наименование = '{counterpartiesOld}'";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void EditProvider(string provider, string providerOld)
        {
            OpenBD();
            string sqlcommand = $@"update Provider set Provider.Наименование = {provider} where Provider.Наименование = '{providerOld}'";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void NameFuelEdit(string oldNameFuel, string newNameFuel)
        {
            OpenBD();
            string sqlcommand = $@"update Fuel set Fuel.[Вид топлива] = {newNameFuel} where Fuel.[Вид топлива] = '{oldNameFuel}'";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void NameFuelTableEdit(string oldNameFuel, string newNameFuel)
        {
            OpenBD();
            string sqlcommandOne = $@"SELECT * INTO {newNameFuel} FROM {oldNameFuel}";
            string sqlcommandTwo = $@"drop table {oldNameFuel}";
            InDB(sqlcommandOne,sqlcommandTwo);
            CloseOpeBD();
        }
        public void NameFuelAndTableEdit(string oldNameFuel, string newNameFuel)
        {
            NameFuelEdit(oldNameFuel, newNameFuel);
            NameFuelTableEdit(oldNameFuel, newNameFuel);
        }
    }
}
