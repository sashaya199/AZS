﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassAll.Encryption;

namespace ClassAll.Queries
{
    public class TestInquiry : InquiryMeaning
    {
        public bool Test()
        {
            string[] fuel = AllFuel();
            bool ret = true;
            foreach (var item in fuel)
            {
                if (QuantityRowsRecordComing(item) == 0)
                {
                    ret = false;
                    MessageBox.Show($"Не заплнена начальные остатки по топливу {item}. Желаете заполнить");
                    break;
                }
                if (QuantityRows(item) <= 2)
                {
                    ret = false;
                    MessageBox.Show($"Не заплнена калибровочная таблица по топливу {item}. Желаете заполнить");
                    break;
                }
            }
            return ret;
        }

        public bool Authorization(string login, string password) // авторизация
        {
            OpenBD();
            Cryption cryption = new Cryption();
            password = cryption.Password(password);
            string tabl = "[Authorization]";
            string commandTxt = @"SELECT * FROM " + tabl + "  WHERE Логин= '" + login + "'AND Пароль= '" + password + "';";
            Treatment(commandTxt, tabl);
            if (Data.Tables[tabl].Rows.Count > 0)
            {
                CloseOpeBD();
                return true;
            }
            else
            {
                MessageBox.Show("Не верный логин или пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public string[] EmptyFuelTable()// вывод списка не заполеных калибровочных таблиц
        {
            OpenBD();
            string[] fuelAll = AllFuel();
            List<string> emptyFuel = new List<string>();
            foreach (var item in fuelAll)
            {
                if (QuantityRows(item) < 2)
                {
                    emptyFuel.Add(item);
                }
            }
            return emptyFuel.ToArray();
        }

        public void ExistenceTableFuel() // Сопосталение 
        {
            OpenBD();
            string[] fuelALL = AllFuel();
            string[] tableFuel = TableFuel();
            var inter = fuelALL.Intersect(tableFuel).ToArray();
            fuelALL = fuelALL.Except(inter).ToArray();
            tableFuel = tableFuel.Except(inter).ToArray();
            int countTable = 0;
            int countFuel = 0;
            using (InquiryInto inquiryInto = new InquiryInto())
            {
                foreach (string item in fuelALL)
                {
                    inquiryInto.CreadTableFuel(item);
                    countFuel++;
                }
                foreach (string item in tableFuel)
                {
                    inquiryInto.AddFuel(item);
                    countTable++;
                }
            } 
            MessageBox.Show($"Сопоставление выявило {countTable + countFuel} ошибок. Созданно {countTable} градуировочных таблиц и {countFuel} видов топлива");
        }
    }

}
