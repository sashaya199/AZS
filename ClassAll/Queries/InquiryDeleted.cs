﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    public class InquiryDeleted : QueriesBD
    {
        public void DeleteRecordComing(string ID)//удаление записи учёта
        {
            OpenBD();
            string sqlcommand = @"DELETE Coming.* FROM Coming WHERE Coming.Код= " + ID;
            InDB(sqlcommand);
        }
        public void DeleteRecordConsumption(string ID)//удаление записи учёта
        {
            OpenBD();
            string sqlcommand = @"DELETE Consumption.* FROM Consumption WHERE Consumption.Код= " + ID;
            InDB(sqlcommand);
        }
        public void DeleteInventoei(string ID)//удаление записи ивентаризация
        {
            OpenBD();
            string sqlcommand = @"delete Inventori.* from Inventori where Inventori.Код= " + ID;
            InDB(sqlcommand);
        }
        public void DeleteCounterparties(string counterparties)//удаление записи контрагента
        {
            OpenBD();
            string sqlcommand = $@"delete Counterparties.* from Counterparties where Counterparties.Наименование = '{counterparties}'";
            InDB(sqlcommand);
        }
        public void DeleteProvider(string provider)//удаление записи поставщика
        {
            OpenBD();
            string sqlcommand = $@"delete Provider.* from Provider where Provider.Наименование = '{provider}'";
            InDB(sqlcommand);
        }
        public void CalibrationDel(string fuel)
        {
            OpenBD();
            string sqlcommand = $"drop table {fuel}";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void FuelDel(string fuel)
        {
            OpenBD();
            string sqlcommand = $"delete Fuel.* from Fuel where Fuel.[Вид топлива] = '{fuel}' ";
            InDB(sqlcommand);
            CloseOpeBD();
        }
    }
}
