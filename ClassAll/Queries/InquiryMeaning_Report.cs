﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    public partial class InquiryMeaning : QueriesBD
    {
        public void ReportInventori(string dateStart, string dateEnd)//запрос для отчёта инвентаризации по всем видам топлива
        {
            OpenBD();
            string sqlcommandDatadrid = $@"select Inventori.Дата, Fuel.[Вид топлива], Inventori.[Фактические мм], Inventori.[Фактические литры], Inventori.[Книжные мм], Inventori.[Книжные литры], Inventori.Расхождение 
                                        from Fuel INNER JOIN Inventori ON Fuel.Id = Inventori.Топливо 
                                        where (Inventori.Дата>= #{dateStart}# And Inventori.Дата<= #{dateEnd}# )";
            string sqlcommand = $@"SELECT Inventori.Дата AS [Date], Fuel.[Вид топлива] AS Fuel, Inventori.[Фактические мм] AS Fmm, Inventori.[Фактические литры] AS Fliters, 
                                Inventori.[Книжные мм] AS Kmm, Inventori.[Книжные литры] AS Kliters, Inventori.Расхождение AS Discrepancy
                                FROM (Inventori INNER JOIN
                                Fuel ON Inventori.Топливо = Fuel.Id)
                                where (Inventori.Дата>= #{dateStart}# And Inventori.Дата<= #{dateEnd}# )";
            Treatment(sqlcommand, "ReportInventori");
            Treatment(sqlcommandDatadrid);
        }
        public void ReportInventori(string dateStart, string dateEnd, string fuel)//запрос для отчёта инвентаризации по определёному виду топлива
        {
            OpenBD();
            string sqlcommandDatadrid = $@"select Inventori.Дата, Fuel.[Вид топлива], Inventori.[Фактические мм], Inventori.[Фактические литры], Inventori.[Книжные мм], Inventori.[Книжные литры], Inventori.Расхождение 
                                        from Fuel INNER JOIN Inventori ON Fuel.Id = Inventori.Топливо 
                                        where (Inventori.Дата>= #{dateStart}# And Inventori.Дата<= #{dateEnd}# )  AND (Fuel.[Вид топлива]= '{fuel}' )";
            string sqlcommand = $@"SELECT Inventori.Дата AS [Date], Fuel.[Вид топлива] AS Fuel, Inventori.[Фактические мм] AS Fmm, Inventori.[Фактические литры] AS Fliters, 
                                Inventori.[Книжные мм] AS Kmm, Inventori.[Книжные литры] AS Kliters, Inventori.Расхождение AS Discrepancy
                                FROM (Inventori INNER JOIN
                                Fuel ON Inventori.Топливо = Fuel.Id)
                                where (Inventori.Дата>= #{dateStart}# And Inventori.Дата<= #{dateEnd}# )  AND (Fuel.[Вид топлива]= '{fuel}' )";
            Treatment(sqlcommand, "ReportInventori");
            Treatment(sqlcommandDatadrid);
        }

        public void ReportRecordFuel(string dateStart, string dateEnd, string fuel)//отчёт прихода по топливу
        {
            OpenBD();
            string sqlcommand = $@"SELECT Coming.Дата, Fuel.[Вид топлива] AS Топливо, Provider.Наименование AS Контрагент, Coming.Литры
                                 FROM ((Coming INNER JOIN Fuel ON Coming.Топливо = Fuel.Id) INNER JOIN  Provider ON Coming.Поставщик = Provider.Id) 
                                 WHERE (Coming.Дата >= #{dateStart}# AND Coming.Дата <= #{dateEnd}#) AND (Fuel.[Вид топлива] = '{fuel}')";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
        public void ReportRecordCounterpartiesConsumption(string dateStart, string dateEnd, string Counterparties)// отчёт расхода по контрагенту
        {
            OpenBD();
            string sqlcommand = $@"SELECT Consumption.Дата, Fuel.[Вид топлива] as Топливо, Counterparties.Наименование AS Контрагент, Consumption.Литры
                                FROM   ((Consumption INNER JOIN  Fuel ON Consumption.Топливо = Fuel.Id) INNER JOIN  Counterparties ON Consumption.Контрагент = Counterparties.Id) 
                                where (Consumption.Дата >= #{dateStart}# and Consumption.Дата <= #{dateEnd}# And Counterparties.Наименование = '{Counterparties}')";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
        public void ReportRecordCounterpartiesComing(string dateStart, string dateEnd, string Counterparties)//отчёт прихода по контрагенту
        {
            OpenBD();
            string sqlcommand = $@"SELECT Coming.Дата, Fuel.[Вид топлива] as Топливо, Provider.Наименование AS Контрагент, Coming.Литры
                                FROM ((Coming INNER JOIN Provider ON Coming.Поставщик = Provider.Id) INNER JOIN  Fuel ON Coming.Топливо = Fuel.Id)
                                where (Coming.Дата >= #{dateStart}# and Coming.Дата <= #{dateEnd}# and Provider.Наименование = '{Counterparties}')";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }

        public void ReportRecordComing(string dateStart, string dateEnd, string fuel)//отчёт прихода по виду топлива
        {
            OpenBD();
            string sqlcommand = $@"SELECT Coming.Дата, Fuel.[Вид топлива] AS Топливо, Provider.Наименование AS Контрагент, Coming.Литры
                                 FROM ((Coming INNER JOIN Fuel ON Coming.Топливо = Fuel.Id) INNER JOIN  Provider ON Coming.Поставщик = Provider.Id) 
                                 WHERE (Coming.Дата >= #{dateStart}# AND Coming.Дата <= #{dateEnd}#) and Fuel.[Вид топлива] = '{fuel}'";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
        public void ReportRecordConsumption(string dateStart, string dateEnd, string fuel)//отчёт расхода по виду топлива
        {
            OpenBD();
            string sqlcommand = $@"SELECT Consumption.Дата, Fuel.[Вид топлива] AS Топливо, Counterparties.Наименование AS Контрагент, Consumption.Литры
                                FROM ((Consumption INNER JOIN Counterparties ON Consumption.Контрагент = Counterparties.Id) INNER JOIN Fuel ON Consumption.Топливо = Fuel.Id)
                                 WHERE (Consumption.Дата >= #{dateStart}# AND Consumption.Дата <= #{dateEnd}#) and Fuel.[Вид топлива] = '{fuel}'";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
        public void ReportRecordConsumptionAll(string dateStart, string dateEnd)//отчёт приода по всему расходу 
        {
            OpenBD();
            string sqlcommand = $@"SELECT Consumption.Дата, Fuel.[Вид топлива] AS Топливо, Counterparties.Наименование AS Контрагент, Consumption.Литры
                                FROM ((Consumption INNER JOIN Counterparties ON Consumption.Контрагент = Counterparties.Id) INNER JOIN Fuel ON Consumption.Топливо = Fuel.Id)
                                 WHERE (Consumption.Дата >= #{dateStart}# AND Consumption.Дата <= #{dateEnd}#)";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
        public void ReportRecordComingAll(string dateStart, string dateEnd)//отчёт приода по всему приходу 
        {
            OpenBD();
            string sqlcommand = $@"SELECT Coming.Дата, Fuel.[Вид топлива] AS Топливо, Provider.Наименование AS Контрагент, Coming.Литры
                                 FROM ((Coming INNER JOIN Fuel ON Coming.Топливо = Fuel.Id) INNER JOIN  Provider ON Coming.Поставщик = Provider.Id) 
                                 WHERE (Coming.Дата >= #{dateStart}# AND Coming.Дата <= #{dateEnd}#)";
            Treatment(sqlcommand, "ReportRecords");
            Treatment(sqlcommand);
        }
    }
}
