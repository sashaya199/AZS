﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using ClassAll.Settings;

namespace ClassAll.Queries
{
    public abstract class QueriesBD : IRequestForDb, IDisposable
    {
        #region IDisposable
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    connection.Close();
                    Data.Dispose();
                    DataTable.Dispose();
                    adapter.Dispose();
                    command.Dispose();
                    connection.Dispose();
                    Msg = null;
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public QueriesBD()
        {
            connection.ConnectionString = new MeaningsSetting().FullStringConnetc();
        }

        private OleDbConnection connection = new OleDbConnection(); // создание обьекта для подключения к базе
        private OleDbCommand command = new OleDbCommand(); // создание обьекта команды 
        private OleDbDataAdapter adapter = new OleDbDataAdapter();// создание обьекта обновления датасет
        private DataSet data = new DataSet();
        private DataTable dataTable = new DataTable();

        public DataSet Data { get => data; private set => data = value; }
        public DataTable DataTable { get => dataTable; private set => dataTable = value; }
        public string Msg { get; private set; }

        public void OpenBD()//подключение к базе
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    command.Connection = connection;
                    connection.Open();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка подключения", "");
            }
        }
        public void CloseOpeBD()// зактрытие подлючения
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void Treatment(string commandTxt, string adapterfilltabl) // метод фильтрации таблицы
        {
            try
            {
                command.CommandText = commandTxt;
                adapter.SelectCommand = command;
                adapter.Fill(Data, adapterfilltabl);
                Msg = "Успешно";
            }
            catch (Exception ex)
            {
                Msg = ex.Message + " ошибка команды";
            }

        }
        public void Treatment(string commandTxt) // метод фильтрации
        {
            try
            {
                DataTable.Clear();
                command.CommandText = commandTxt;
                adapter.SelectCommand = command;
                adapter.Fill(DataTable);
                Msg = "Успешно";
            }
            catch (Exception ex)
            {
                Msg = $"ошибка {ex}";
            }

        }
        public void InDB(string sqlcommand)// добавление в базу
        {
            try
            {
                command.CommandText = sqlcommand;
                command.ExecuteNonQuery();
                Msg = "Успешно";
            }
            catch (InvalidOperationException)
            {
                Msg = "Ошибка";
            }
        }

        public void InDB(string sqlcommandOne, string sqlcommandTwo)// добавление в базу
        {
            try
            {
                command.CommandText = sqlcommandOne;
                command.ExecuteNonQuery();
                command.CommandText = sqlcommandTwo;
                command.ExecuteNonQuery();
                Msg = "Успешно";
            }
            catch (Exception e)
            {
                Msg = $"Ошибка {e}";
            }

        }
        public string SingleValueColum(string sqlcommand, string columName) // вывод первого значения из колонки
        {
            command.CommandText = sqlcommand;
            using (OleDbDataReader reader = command.ExecuteReader())
            {
                try
                {
                    reader.Read();
                    string value = reader[columName].ToString();
                    reader.Close();
                    Msg = "Успешно";
                    return value;
                }
                catch (Exception e)
                {
                    Msg = $"Ошибка чтения {e}";
                    return "";
                }

            };
        }
        public string[] ValueColum(string sqlcommand, string columName) // вывод коллонки в массиве
        {
            command.CommandText = sqlcommand;
            using (OleDbDataReader reader = command.ExecuteReader())
            {
                List<string> list = new List<string>();
                while (reader.Read())
                {
                    list.Add(reader[columName].ToString());
                }
                reader.Close();
                return list.ToArray();
            }
        }
        public string[] ListAllTable()//список всех таблиц
        {
            List<string> tableList = new List<string>();
            DataTable = connection.GetSchema("Tables", new string[] { null, null, null, "TABLE" });
            foreach (DataRow row in DataTable.Rows)
            {
                tableList.Add(row["TABLE_NAME"].ToString());
            };
            return tableList.ToArray();
        }


    }
}

