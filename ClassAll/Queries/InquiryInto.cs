﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    public class InquiryInto : QueriesBD
    {
        public void AddFuel(string Fuel) // добавление нового вида топлива
        {
            OpenBD();
            string sqlAddFuel = $@"INSERT INTO Fuel ([Вид топлива]) values ('{Fuel}')";
            InDB(sqlAddFuel);
        }
        public void CreadTableFuel(string Fuel)// ссоздание градуировочной таблицы
        {
            OpenBD();
            string sqlAddTabl = $@"create table {Fuel} (Миллиметры Single, Литры Single)";
            InDB(sqlAddTabl);
        }
        public void CreadFuel(string fuel)
        {
            CreadTableFuel(fuel);
            AddFuel(fuel);
        }
        public void InventoriAdd(string date, string fuel, string fmm, string fliters, string kmm, string kliters, string discrepancy) // запись инвенторизации в базу
        {
            OpenBD();
            using (InquiryMeaning renewalMeaning = new InquiryMeaning())
            {
                fuel = renewalMeaning.FuelId(fuel);
            }
            string sqlcommand = @"insert into Inventori ( Дата, Топливо, [Фактические мм], [Фактические литры], [Книжные мм], [Книжные литры], Расхождение) values" +
           " ( '" + date + "' , '" + fuel + "' , '" + fmm + "' , '" + fliters + "' , '" + kmm + "' , '" + kliters + "' , '" + discrepancy + "' );";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void RegistrationComing(string date, string fuel, string Provider, string liters)//запись прихода
        {
            OpenBD();
            InquiryMeaning renewalMeaning = new InquiryMeaning();
            fuel = renewalMeaning.FuelId(fuel);
            Provider = renewalMeaning.ProviderID(Provider);
            string sqlcommand = $@"insert into Coming (Дата, Топливо, Поставщик, Литры)
                                values ('{date}', '{fuel}', '{Provider}', '{liters}')";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void RegistrationConsumption(string date, string fuel, string counterparties, string liters)//запись расхода
        {
            OpenBD();
            InquiryMeaning renewalMeaning = new InquiryMeaning();
            fuel = renewalMeaning.FuelId(fuel);
            counterparties = renewalMeaning.CounterpartiesID(counterparties);//
            string sqlcommand = $@"insert into Consumption (Дата, Топливо, Контрагент, Литры)
                                values ('{date}', '{fuel}', '{counterparties}', '{liters}')";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void Counterparties(string counterparties)//добавление контерагента
        {
            OpenBD();
            string sqlcommand = $@"insert into Counterparties (Наименование) values ('{counterparties}')";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void Provider(string Provider)// добавление поставщика
        {
            OpenBD();
            string sqlcommand = $@"insert into Provider (Наименование) values ('{Provider}')";
            InDB(sqlcommand);
            CloseOpeBD();
        }
        public void CalibrationAdd(string valueMM, string valueLiters, string tabl)
        {
            OpenBD();
            string sqlcommand = $@"insert into {tabl} (Миллиметры, Литры) values ('{valueMM}', '{valueLiters}')";
            InDB(sqlcommand);
        }

    }
}
