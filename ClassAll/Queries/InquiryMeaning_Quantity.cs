﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassAll.Queries
{
    public partial class InquiryMeaning : QueriesBD
    {
        public int QuantityRows(string tabl)// подсчёт записей в таблице с условием
        {
            OpenBD();
            string sqlqueries = $"SELECT top 10 {tabl}.* FROM {tabl}"; //top 10 для уменьшение времени расчёта
            Treatment(sqlqueries, tabl);
            int coun = Data.Tables[tabl].Rows.Count;
            return coun;
        }
        public int QuantityRowsRecordComing(string fuel)// подсчёт записей по виду топлива
        {
            OpenBD();
            string sqlqueries = $@"SELECT top 1 Coming.Литры, Fuel.[Вид топлива] FROM Fuel INNER JOIN Coming ON Fuel.Id = Coming.Топливо WHERE Coming.Литры > 0 AND Fuel.[Вид топлива]= '{fuel}';"; // топ 1 сделан для быстрой обработки
            Treatment(sqlqueries, "Coming");
            int coun = Data.Tables["Coming"].Rows.Count;
            return coun;
        }
        public int QuantityRowsRecordConsumption(string fuel)// подсчёт записей по виду топлива
        {
            OpenBD();
            string sqlqueries = $@"SELECT Consumption.Литры, Fuel.[Вид топлива] FROM Fuel INNER JOIN Consumption ON Fuel.Id = Consumption.Топливо WHERE (Fuel.[Вид топлива]= '{fuel}');"; // топ 1 сделан для быстрой обработки
            Treatment(sqlqueries, "Consumption");
            int coun = Data.Tables["Consumption"].Rows.Count;
            return coun;
        }
    }
}
