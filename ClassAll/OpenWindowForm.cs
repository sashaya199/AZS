﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClassAll
{
    public class OpenWindowForm
    {
        public void OpenForms<FormOpen>()
            where FormOpen : Form, new()
        {
            FormOpen form = new FormOpen();
            int count = 0;
            foreach (Form item in Application.OpenForms)
            {
                if (form.Name == item.Name)
                {
                    count++;
                    form.Activate();
                    break;
                }
            }
            if (count == 0)
            {
                form.Show();
            }
        }
    }
}
