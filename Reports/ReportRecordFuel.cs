﻿using System;
using ClassAll.Queries;
using WeifenLuo.WinFormsUI.Docking;

namespace Reports
{
    public partial class ReportRecordFuel : DockContent
    {
        public ReportRecordFuel()
        {
            InitializeComponent();
        }

        private void ReportRecord_Load(object sender, EventArgs e)
        {
            InquiryMeaning renewal = new InquiryMeaning();
            FuelBox.Items.AddRange(renewal.AllFuel());
            renewal.Dispose();
            FuelBox.SelectedIndex = 0;
            DateEnd.Value = DateTime.Today;
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            DateStart.Value = startDate;
            UpDataGrid();
        }
        private void ButtonIn_Click(object sender, EventArgs e)
        {

            UpDataGrid();
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                Variable(renewal);
                new VariableReport().ReportShow("Reports.ReportRecords.rdlc", "ReportRecords", renewal);
            }
        }

        private void UpDataGrid()
        {
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                renewal.DataTable.Clear();
                Variable(renewal);
                DataGrid.DataSource = renewal.DataTable;
            }
        }

        private void Variable(InquiryMeaning renewal)
        {
            if (Coming.Checked == true)
            {
                if (FuelСheckBox.Checked == true)
                {
                    renewal.ReportRecordComingAll(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"));
                }
                else
                {
                    renewal.ReportRecordComing(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"), FuelBox.Text);
                }
            }
            else if (Consumption.Checked == true)
            {
                if (FuelСheckBox.Checked == true)
                {
                    renewal.ReportRecordConsumptionAll(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"));
                }
                else
                {
                    renewal.ReportRecordConsumption(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"), FuelBox.Text);
                }
            }
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FuelСheckBox.Checked == true)
            {
                FuelBox.Enabled = false;
            }
            else
            {
                FuelBox.Enabled = true;
            }
        }

        private void ButtonWred_Click(object sender, EventArgs e)
        {
            UpDataGrid();
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FuelСheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FuelСheckBox.Checked == true)
            {
                FuelBox.Enabled = false;
            }
            else
            {
                FuelBox.Enabled = true;
            }
        }
    }
}
