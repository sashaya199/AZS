﻿namespace Reports
{
    partial class ReportRecordConsumption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportRecordConsumption));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.DataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.ButtonWred = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateEnd = new System.Windows.Forms.DateTimePicker();
            this.DateStart = new System.Windows.Forms.DateTimePicker();
            this.CounterpartiesBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CheckCounterparties = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Coming = new System.Windows.Forms.RadioButton();
            this.Consumption = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(848, 620);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 13;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // DataGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGrid.BackgroundColor = System.Drawing.Color.DarkGray;
            this.DataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGrid.DoubleBuffered = true;
            this.DataGrid.EnableHeadersVisualStyles = false;
            this.DataGrid.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.DataGrid.HeaderForeColor = System.Drawing.Color.Black;
            this.DataGrid.Location = new System.Drawing.Point(12, 129);
            this.DataGrid.Name = "DataGrid";
            this.DataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGrid.Size = new System.Drawing.Size(940, 483);
            this.DataGrid.TabIndex = 14;
            // 
            // ButtonWred
            // 
            this.ButtonWred.ActiveBorderThickness = 1;
            this.ButtonWred.ActiveCornerRadius = 20;
            this.ButtonWred.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonWred.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonWred.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonWred.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonWred.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonWred.BackgroundImage")));
            this.ButtonWred.ButtonText = "Вывод данных";
            this.ButtonWred.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonWred.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonWred.ForeColor = System.Drawing.Color.Black;
            this.ButtonWred.IdleBorderThickness = 1;
            this.ButtonWred.IdleCornerRadius = 20;
            this.ButtonWred.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonWred.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonWred.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonWred.Location = new System.Drawing.Point(669, 28);
            this.ButtonWred.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonWred.Name = "ButtonWred";
            this.ButtonWred.Size = new System.Drawing.Size(116, 40);
            this.ButtonWred.TabIndex = 22;
            this.ButtonWred.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonWred.Click += new System.EventHandler(this.ButtonWred_Click);
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Печать";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(669, 78);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(116, 40);
            this.ButtonIn.TabIndex = 23;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 19);
            this.label2.TabIndex = 19;
            this.label2.Text = "До:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 19);
            this.label1.TabIndex = 17;
            this.label1.Text = "От:";
            // 
            // DateEnd
            // 
            this.DateEnd.CustomFormat = "dd/MM/yyyy";
            this.DateEnd.Location = new System.Drawing.Point(355, 35);
            this.DateEnd.Margin = new System.Windows.Forms.Padding(4);
            this.DateEnd.Name = "DateEnd";
            this.DateEnd.Size = new System.Drawing.Size(184, 27);
            this.DateEnd.TabIndex = 16;
            this.DateEnd.Value = new System.DateTime(2018, 3, 3, 11, 53, 5, 0);
            // 
            // DateStart
            // 
            this.DateStart.CustomFormat = "dd/MM/yyyy";
            this.DateStart.Location = new System.Drawing.Point(77, 37);
            this.DateStart.Margin = new System.Windows.Forms.Padding(4);
            this.DateStart.Name = "DateStart";
            this.DateStart.Size = new System.Drawing.Size(218, 27);
            this.DateStart.TabIndex = 15;
            this.DateStart.Value = new System.DateTime(2018, 3, 3, 11, 53, 9, 0);
            // 
            // CounterpartiesBox
            // 
            this.CounterpartiesBox.DisplayMember = "Вид топлива";
            this.CounterpartiesBox.FormattingEnabled = true;
            this.CounterpartiesBox.Location = new System.Drawing.Point(136, 85);
            this.CounterpartiesBox.Name = "CounterpartiesBox";
            this.CounterpartiesBox.Size = new System.Drawing.Size(159, 27);
            this.CounterpartiesBox.TabIndex = 20;
            this.CounterpartiesBox.ValueMember = "Вид топлива";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 19);
            this.label4.TabIndex = 18;
            this.label4.Text = "Контрагенты:";
            // 
            // CheckCounterparties
            // 
            this.CheckCounterparties.AutoSize = true;
            this.CheckCounterparties.Location = new System.Drawing.Point(301, 85);
            this.CheckCounterparties.Name = "CheckCounterparties";
            this.CheckCounterparties.Size = new System.Drawing.Size(151, 23);
            this.CheckCounterparties.TabIndex = 21;
            this.CheckCounterparties.Text = "Все контрагенты";
            this.CheckCounterparties.UseVisualStyleBackColor = true;
            this.CheckCounterparties.CheckedChanged += new System.EventHandler(this.CheckCounterparties_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Coming);
            this.groupBox1.Controls.Add(this.Consumption);
            this.groupBox1.Location = new System.Drawing.Point(546, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 86);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Вид ";
            // 
            // Coming
            // 
            this.Coming.AutoSize = true;
            this.Coming.Location = new System.Drawing.Point(7, 57);
            this.Coming.Name = "Coming";
            this.Coming.Size = new System.Drawing.Size(80, 26);
            this.Coming.TabIndex = 1;
            this.Coming.Text = "Приход";
            this.Coming.UseCompatibleTextRendering = true;
            this.Coming.UseVisualStyleBackColor = true;
            this.Coming.CheckedChanged += new System.EventHandler(this.Coming_CheckedChanged);
            // 
            // Consumption
            // 
            this.Consumption.AutoSize = true;
            this.Consumption.Checked = true;
            this.Consumption.Location = new System.Drawing.Point(7, 27);
            this.Consumption.Name = "Consumption";
            this.Consumption.Size = new System.Drawing.Size(75, 26);
            this.Consumption.TabIndex = 0;
            this.Consumption.TabStop = true;
            this.Consumption.Text = "Расход";
            this.Consumption.UseCompatibleTextRendering = true;
            this.Consumption.UseVisualStyleBackColor = true;
            this.Consumption.CheckedChanged += new System.EventHandler(this.Coming_CheckedChanged);
            // 
            // ReportRecordConsumption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 674);
            this.CloseButtonVisible = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonWred);
            this.Controls.Add(this.ButtonIn);
            this.Controls.Add(this.CheckCounterparties);
            this.Controls.Add(this.CounterpartiesBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateEnd);
            this.Controls.Add(this.DateStart);
            this.Controls.Add(this.DataGrid);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReportRecordConsumption";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Document;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчёт по контрагенту";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private Bunifu.Framework.UI.BunifuCustomDataGrid DataGrid;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonWred;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DateEnd;
        private System.Windows.Forms.DateTimePicker DateStart;
        private System.Windows.Forms.ComboBox CounterpartiesBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox CheckCounterparties;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Coming;
        private System.Windows.Forms.RadioButton Consumption;
    }
}