﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassAll.Queries;
using System.Text;
using Microsoft.Reporting.WinForms;

namespace Reports
{
    class VariableReport
    {
        public void ReportShow(string reportName, string tableName, InquiryMeaning inquiryMeaning)
        {
            Report report = new Report();
            report.ViewerReport.LocalReport.ReportEmbeddedResource = reportName;
            report.ViewerReport.LocalReport.DataSources.Clear();
            report.ViewerReport.LocalReport.DataSources.Add(new ReportDataSource(tableName, inquiryMeaning.Data.Tables[tableName]));
            report.ViewerReport.RefreshReport();
            report.Show();
        }
    }
}
