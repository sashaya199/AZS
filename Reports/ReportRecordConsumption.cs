﻿using System;
using ClassAll.Queries;
using WeifenLuo.WinFormsUI.Docking;

namespace Reports
{
    public partial class ReportRecordConsumption : DockContent
    {
        public ReportRecordConsumption()
        {
            InitializeComponent();
        }

        private void ReportRecord_Load(object sender, EventArgs e)
        {
            DateEnd.Value = DateTime.Today;
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            DateStart.Value = startDate;
            UpdataControl();
            UpDataGrid();
        }
        private void ButtonIn_Click(object sender, EventArgs e)
        {
            UpDataGrid();
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                Report report = new Report();
                Variable(renewal);
                new VariableReport().ReportShow("Reports.ReportRecords.rdlc", "ReportRecords", renewal);
            }
        }

        private void UpDataGrid()
        {
            InquiryMeaning renewal = new InquiryMeaning();
            renewal.DataTable.Clear();
            Variable(renewal);
            DataGrid.DataSource = renewal.DataTable;
        }
        private void UpdataControl()
        {
            CounterpartiesBox.Items.Clear();
            using (InquiryMeaning inquiry = new InquiryMeaning())
            {
                if (Coming.Checked == true)
                {
                    CounterpartiesBox.Items.AddRange(inquiry.Provider());
                }
                else
                {
                    CounterpartiesBox.Items.AddRange(inquiry.Counterparties());
                }
            }
            CounterpartiesBox.SelectedIndex = 0;
        }

        private void Variable(InquiryMeaning renewal)//
        {
            if (Coming.Checked==true)
            {
                if (CheckCounterparties.Checked == true)
                {
                    renewal.ReportRecordComingAll(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"));
                }
                else
                {
                    renewal.ReportRecordCounterpartiesComing(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"), CounterpartiesBox.Text);
                }
            }
            else if (Consumption.Checked==true)
            {
                if (CheckCounterparties.Checked == true)
                {
                    renewal.ReportRecordConsumptionAll(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"));
                }
                else
                {
                    renewal.ReportRecordCounterpartiesConsumption(DateStart.Value.Date.ToString("dd-MM-yyyy"), DateEnd.Value.Date.ToString("dd-MM-yyyy"), CounterpartiesBox.Text);
                }
            }
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CheckCounterparties_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckCounterparties.Checked == true)
            {
                CounterpartiesBox.Enabled = false;
            }
            else
            {
                CounterpartiesBox.Enabled = true;
            }
            UpDataGrid();
        }

        private void Coming_CheckedChanged(object sender, EventArgs e)
        {
            UpdataControl();
            UpDataGrid();
        }

        private void ButtonWred_Click(object sender, EventArgs e)
        {
            UpDataGrid();
        }
    }
}
