﻿namespace Reports
{
    partial class ReportsInventori
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportsInventori));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DateStart = new System.Windows.Forms.DateTimePicker();
            this.DateEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FuelBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FuelСheckBox = new System.Windows.Forms.CheckBox();
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonWred = new Bunifu.Framework.UI.BunifuThinButton2();
            this.DataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // DateStart
            // 
            this.DateStart.CustomFormat = "dd/MM/yyyy";
            this.DateStart.Location = new System.Drawing.Point(87, 31);
            this.DateStart.Margin = new System.Windows.Forms.Padding(4);
            this.DateStart.Name = "DateStart";
            this.DateStart.Size = new System.Drawing.Size(190, 27);
            this.DateStart.TabIndex = 0;
            this.DateStart.Value = new System.DateTime(2018, 3, 3, 11, 53, 9, 0);
            // 
            // DateEnd
            // 
            this.DateEnd.CustomFormat = "dd/MM/yyyy";
            this.DateEnd.Location = new System.Drawing.Point(342, 31);
            this.DateEnd.Margin = new System.Windows.Forms.Padding(4);
            this.DateEnd.Name = "DateEnd";
            this.DateEnd.Size = new System.Drawing.Size(184, 27);
            this.DateEnd.TabIndex = 1;
            this.DateEnd.Value = new System.DateTime(2018, 3, 3, 11, 53, 5, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "От:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "До:";
            // 
            // FuelBox
            // 
            this.FuelBox.DisplayMember = "Вид топлива";
            this.FuelBox.FormattingEnabled = true;
            this.FuelBox.Location = new System.Drawing.Point(180, 75);
            this.FuelBox.Name = "FuelBox";
            this.FuelBox.Size = new System.Drawing.Size(121, 27);
            this.FuelBox.TabIndex = 4;
            this.FuelBox.ValueMember = "Вид топлива";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "Вид топлива:";
            // 
            // FuelСheckBox
            // 
            this.FuelСheckBox.AutoSize = true;
            this.FuelСheckBox.Location = new System.Drawing.Point(325, 79);
            this.FuelСheckBox.Name = "FuelСheckBox";
            this.FuelСheckBox.Size = new System.Drawing.Size(159, 23);
            this.FuelСheckBox.TabIndex = 9;
            this.FuelСheckBox.Text = "Все виды топлива";
            this.FuelСheckBox.UseVisualStyleBackColor = true;
            this.FuelСheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Печать";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(534, 68);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(116, 40);
            this.ButtonIn.TabIndex = 10;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // ButtonWred
            // 
            this.ButtonWred.ActiveBorderThickness = 1;
            this.ButtonWred.ActiveCornerRadius = 20;
            this.ButtonWred.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonWred.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonWred.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonWred.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonWred.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonWred.BackgroundImage")));
            this.ButtonWred.ButtonText = "Вывод данных";
            this.ButtonWred.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonWred.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonWred.ForeColor = System.Drawing.Color.Black;
            this.ButtonWred.IdleBorderThickness = 1;
            this.ButtonWred.IdleCornerRadius = 20;
            this.ButtonWred.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonWred.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonWred.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonWred.Location = new System.Drawing.Point(534, 18);
            this.ButtonWred.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonWred.Name = "ButtonWred";
            this.ButtonWred.Size = new System.Drawing.Size(116, 40);
            this.ButtonWred.TabIndex = 10;
            this.ButtonWred.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonWred.Click += new System.EventHandler(this.ButtonWred_Click);
            // 
            // DataGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGrid.BackgroundColor = System.Drawing.Color.DarkGray;
            this.DataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGrid.DoubleBuffered = true;
            this.DataGrid.EnableHeadersVisualStyles = false;
            this.DataGrid.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.DataGrid.HeaderForeColor = System.Drawing.Color.Black;
            this.DataGrid.Location = new System.Drawing.Point(11, 147);
            this.DataGrid.Name = "DataGrid";
            this.DataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGrid.Size = new System.Drawing.Size(907, 409);
            this.DataGrid.TabIndex = 11;
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(814, 564);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 12;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ReportsInventori
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 618);
            this.CloseButtonVisible = false;
            this.Controls.Add(this.ButtonOut);
            this.Controls.Add(this.DataGrid);
            this.Controls.Add(this.ButtonWred);
            this.Controls.Add(this.ButtonIn);
            this.Controls.Add(this.FuelСheckBox);
            this.Controls.Add(this.FuelBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateEnd);
            this.Controls.Add(this.DateStart);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReportsInventori";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчёт инвентаризация";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportsInventori_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateStart;
        private System.Windows.Forms.DateTimePicker DateEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox FuelBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox FuelСheckBox;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonWred;
        private Bunifu.Framework.UI.BunifuCustomDataGrid DataGrid;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
    }
}