﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Queries;
using ClassAll;
using System.Windows.Forms;

namespace Record.Calibration
{
    public partial class FuelTableCalibration : Form
    {
        public FuelTableCalibration()
        {
            InitializeComponent();
        }

        private void FuelTableCalibration_Load(object sender, EventArgs e)
        {
            Updata();
        }
        private void Updata()
        {
            string[] tableFuel;
            string[] allFuel;
            using (InquiryMeaning inquiryMeaning = new InquiryMeaning())
            {
                tableFuel = inquiryMeaning.TableFuel();
                allFuel = inquiryMeaning.AllFuel();
            }
            Array.Sort(tableFuel);
            Array.Sort(allFuel);
            FuelBox.Items.Clear();
            CalibrationBox.Items.Clear();
            FuelBox.Items.AddRange(allFuel);
            CalibrationBox.Items.AddRange(tableFuel);
            if (FuelBox.Items.Count > 0)
            {
                FuelBox.SelectedIndex = 0;
            }
            if (CalibrationBox.Items.Count > 0)
            {
                CalibrationBox.SelectedIndex = 0;
            }
        }

        private void FuelBox_SelectedIndexChanged(object sender, EventArgs e) // баг с нажатием 
        {
            int sItem = -1;
            if (FuelBox.SelectedItem != null)
            {
                sItem = CalibrationBox.FindStringExact(FuelBox.SelectedItem.ToString());
            }
            if (sItem == -1)
            {
                CalibrationBox.SelectedIndex = sItem;
            }
            else
            {
                CalibrationBox.SelectedIndex = sItem;
            }
        }

        private void CalibrationBox_SelectedIndexChanged(object sender, EventArgs e)// баг с нажатием 
        {
            int sItem = -1;
            if (CalibrationBox.SelectedItem != null)
            {
                sItem = FuelBox.FindStringExact(CalibrationBox.SelectedItem.ToString());
            }
            if (sItem == -1)
            {
                FuelBox.SelectedIndex = sItem;
            }
            else
            {
                FuelBox.SelectedIndex = sItem;
            }
        }

        private void ButtonRel_Click(object sender, EventArgs e)
        {
            Updata();
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BunifuThinButton21_Click(object sender, EventArgs e)
        {
            new TestInquiry().ExistenceTableFuel();
            Updata();
        }

        private void FuelButtonAdd_Click(object sender, EventArgs e)
        {
            new AddFuel().ShowDialog();
            Updata();
        }

        private void ТопливоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InquiryDeleted inquiryDeleted = new InquiryDeleted();
            if (String.IsNullOrEmpty(FuelBox.SelectedItem.ToString()))
            {
                MessageBox.Show("Не выбран вид топлива");
            }
            else
            {
                inquiryDeleted.FuelDel(CalibrationBox.SelectedItem.ToString());
            }
            Updata();
        }

        private void ГрадуировочнуюТаблицуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InquiryDeleted inquiryDeleted = new InquiryDeleted();
            if (String.IsNullOrEmpty(CalibrationBox.SelectedItem.ToString()))
            {
                MessageBox.Show("Не выбранна градуировочная таблица");
            }
            else
            {
                inquiryDeleted.CalibrationDel(CalibrationBox.SelectedItem.ToString());
            }
            Updata();
        }

        private void ВсёToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InquiryDeleted inquiryDeleted = new InquiryDeleted();
            if (CalibrationBox.SelectedItem != null | FuelBox.SelectedItem != null)
            {
                MessageBox.Show("Не выбранна градуировочная таблица или топливо");
            }
            else
            {
                inquiryDeleted.CalibrationDel(CalibrationBox.SelectedItem.ToString());
                inquiryDeleted.FuelDel(FuelBox.SelectedItem.ToString());
            }
            Updata();
        }

        private void ПросмотретьТаблицуToolStripMenuItem_Click(object sender, EventArgs e)
        {

            new ViewCalibration(CalibrationBox.SelectedItem.ToString()).Show();
        }

        private void ГрадуировочнуюТаблицуToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (CalibrationBox.SelectedIndex != -1)
            {
                new EditNameFuel(CalibrationBox.SelectedItem.ToString(), "Таблица").ShowDialog();
                Updata();
            }
        }

        private void ТопливоToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (FuelBox.SelectedIndex != -1)
            {
                new EditNameFuel(FuelBox.SelectedItem.ToString(), "Топливо").ShowDialog();
                Updata();
            }

        }

        private void ТопливоИТаблицуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FuelBox.SelectedIndex != -1 & CalibrationBox.SelectedIndex != -1)
            {
                new EditNameFuel(FuelBox.SelectedItem.ToString(), "Топливо и таблица").ShowDialog();
                Updata();
            }
        }
    }
}
