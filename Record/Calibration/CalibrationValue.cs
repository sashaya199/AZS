﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Queries;
using System.Windows.Forms;

namespace Record.Calibration
{
    public partial class CalibrationValue : Form
    {
        public CalibrationValue()
        {
            InitializeComponent();
        }

        private void ButtonOut_Click(object sender, EventArgs e) => Close();

        private void ButtonIn_Click(object sender, EventArgs e)
        {
            // провекра данных сделать
            if (String.IsNullOrEmpty(MinMM.Text) & String.IsNullOrEmpty(MaxMM.Text) & String.IsNullOrEmpty(Step.Text) & String.IsNullOrEmpty(FuelComboBox.Text))
            {
                MessageBox.Show("Не все данные введены");
            }
            else
            {
                new CalibrationFill(MinMM.Text,MaxMM.Text, Step.Text, FuelComboBox.Text,TypeStep.Text).ShowDialog();
                Close();
            }
        }

        private void MinMM_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void Calibration_Load(object sender, EventArgs e)
        {
            using (TestInquiry testInquiry = new TestInquiry())
            {
                FuelComboBox.Items.AddRange(testInquiry.EmptyFuelTable());
            }
            TypeStep.SelectedIndex = 0;
            FuelComboBox.SelectedIndex = 0;
        }

        private void FuelComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

    }
}
