﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Queries;
using System.Windows.Forms;

namespace Record.Calibration
{
    public partial class EditNameFuel : Form
    {
        public EditNameFuel(string oldName,string variable)
        {
            InitializeComponent();
            this.oldName = oldName;
            this.variable = variable;
        }
        private string oldName;
        private string variable;

        private string OldName { get => oldName;}
        public string Variable { get => variable;}

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            using (RenevalInquiry reneval = new RenevalInquiry())
            {
                switch (Variable)
                {
                    case "Топливо":
                        reneval.NameFuelEdit(OldName, textBox1.Text);
                        break;
                    case "Таблица":
                        reneval.NameFuelTableEdit(OldName, textBox1.Text);
                        break;
                    case "Топливо и таблица":
                        reneval.NameFuelAndTableEdit(OldName, textBox1.Text);
                        break;
                    default:
                        break;
                }
            }
        }

        private void EditNameFuel_Load(object sender, EventArgs e)
        {
            OldFuelNameLabel.Text = OldName;
        }
    }
}
