﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Queries;
using System.Windows.Forms;

namespace Record.Calibration
{
    public partial class ViewCalibration : Form
    {
        public ViewCalibration(string fuel)
        {
            InitializeComponent();
            Fuel = fuel;
        }
        private string fuel;

        private string Fuel { get => fuel; set => fuel = value; }

        private void ViewCalibration_Load(object sender, EventArgs e)
        {
            Text = $"Градуировочная таблица {Fuel}";
            InquiryMeaning inquiryMeaning = new InquiryMeaning();
            inquiryMeaning.CalibralionFuel(Fuel);
            DataGrid.DataSource = inquiryMeaning.DataTable;
        }
    }
}
