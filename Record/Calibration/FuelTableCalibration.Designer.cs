﻿namespace Record.Calibration
{
    partial class FuelTableCalibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelTableCalibration));
            this.FuelBox = new System.Windows.Forms.ListBox();
            this.CalibrationBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сопоставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переименоватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.топливоToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.градуировочнуюТаблицуToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.топливоИТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.топливоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.градуировочнуюТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.всёToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просмотретьТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FuelBox
            // 
            this.FuelBox.FormattingEnabled = true;
            this.FuelBox.ItemHeight = 19;
            this.FuelBox.Location = new System.Drawing.Point(12, 57);
            this.FuelBox.Name = "FuelBox";
            this.FuelBox.ScrollAlwaysVisible = true;
            this.FuelBox.Size = new System.Drawing.Size(251, 213);
            this.FuelBox.TabIndex = 0;
            this.FuelBox.SelectedIndexChanged += new System.EventHandler(this.FuelBox_SelectedIndexChanged);
            // 
            // CalibrationBox
            // 
            this.CalibrationBox.FormattingEnabled = true;
            this.CalibrationBox.ItemHeight = 19;
            this.CalibrationBox.Location = new System.Drawing.Point(269, 57);
            this.CalibrationBox.Name = "CalibrationBox";
            this.CalibrationBox.ScrollAlwaysVisible = true;
            this.CalibrationBox.Size = new System.Drawing.Size(251, 213);
            this.CalibrationBox.TabIndex = 1;
            this.CalibrationBox.SelectedIndexChanged += new System.EventHandler(this.CalibrationBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Топливо";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Градуировочные таблицы";
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Выход";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(568, 231);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 3;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.сопоставитьToolStripMenuItem,
            this.обновитьToolStripMenuItem,
            this.переименоватьToolStripMenuItem,
            this.удалитьToolStripMenuItem,
            this.просмотретьТаблицуToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(685, 26);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(86, 22);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.FuelButtonAdd_Click);
            // 
            // сопоставитьToolStripMenuItem
            // 
            this.сопоставитьToolStripMenuItem.Name = "сопоставитьToolStripMenuItem";
            this.сопоставитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.сопоставитьToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.сопоставитьToolStripMenuItem.Text = "Сопоставить";
            this.сопоставитьToolStripMenuItem.Click += new System.EventHandler(this.BunifuThinButton21_Click);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.ButtonRel_Click);
            // 
            // переименоватьToolStripMenuItem
            // 
            this.переименоватьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.топливоToolStripMenuItem1,
            this.градуировочнуюТаблицуToolStripMenuItem1,
            this.топливоИТаблицуToolStripMenuItem});
            this.переименоватьToolStripMenuItem.Name = "переименоватьToolStripMenuItem";
            this.переименоватьToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.переименоватьToolStripMenuItem.Text = "Переименовать";
            // 
            // топливоToolStripMenuItem1
            // 
            this.топливоToolStripMenuItem1.Name = "топливоToolStripMenuItem1";
            this.топливоToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.топливоToolStripMenuItem1.Size = new System.Drawing.Size(301, 22);
            this.топливоToolStripMenuItem1.Text = "Топливо";
            this.топливоToolStripMenuItem1.Click += new System.EventHandler(this.ТопливоToolStripMenuItem1_Click);
            // 
            // градуировочнуюТаблицуToolStripMenuItem1
            // 
            this.градуировочнуюТаблицуToolStripMenuItem1.Name = "градуировочнуюТаблицуToolStripMenuItem1";
            this.градуировочнуюТаблицуToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.градуировочнуюТаблицуToolStripMenuItem1.Size = new System.Drawing.Size(301, 22);
            this.градуировочнуюТаблицуToolStripMenuItem1.Text = "Градуировочную таблицу";
            this.градуировочнуюТаблицуToolStripMenuItem1.Click += new System.EventHandler(this.ГрадуировочнуюТаблицуToolStripMenuItem1_Click);
            // 
            // топливоИТаблицуToolStripMenuItem
            // 
            this.топливоИТаблицуToolStripMenuItem.Name = "топливоИТаблицуToolStripMenuItem";
            this.топливоИТаблицуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.E)));
            this.топливоИТаблицуToolStripMenuItem.Size = new System.Drawing.Size(301, 22);
            this.топливоИТаблицуToolStripMenuItem.Text = "Топливо и таблицу";
            this.топливоИТаблицуToolStripMenuItem.Click += new System.EventHandler(this.ТопливоИТаблицуToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.топливоToolStripMenuItem,
            this.градуировочнуюТаблицуToolStripMenuItem,
            this.всёToolStripMenuItem});
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(77, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // топливоToolStripMenuItem
            // 
            this.топливоToolStripMenuItem.Name = "топливоToolStripMenuItem";
            this.топливоToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Delete)));
            this.топливоToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.топливоToolStripMenuItem.Text = "Топливо";
            this.топливоToolStripMenuItem.Click += new System.EventHandler(this.ТопливоToolStripMenuItem_Click);
            // 
            // градуировочнуюТаблицуToolStripMenuItem
            // 
            this.градуировочнуюТаблицуToolStripMenuItem.Name = "градуировочнуюТаблицуToolStripMenuItem";
            this.градуировочнуюТаблицуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.градуировочнуюТаблицуToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.градуировочнуюТаблицуToolStripMenuItem.Text = "Градуировочную таблицу";
            this.градуировочнуюТаблицуToolStripMenuItem.Click += new System.EventHandler(this.ГрадуировочнуюТаблицуToolStripMenuItem_Click);
            // 
            // всёToolStripMenuItem
            // 
            this.всёToolStripMenuItem.Name = "всёToolStripMenuItem";
            this.всёToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Delete)));
            this.всёToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.всёToolStripMenuItem.Text = "Всё";
            this.всёToolStripMenuItem.Click += new System.EventHandler(this.ВсёToolStripMenuItem_Click);
            // 
            // просмотретьТаблицуToolStripMenuItem
            // 
            this.просмотретьТаблицуToolStripMenuItem.Name = "просмотретьТаблицуToolStripMenuItem";
            this.просмотретьТаблицуToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.просмотретьТаблицуToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.просмотретьТаблицуToolStripMenuItem.Text = "Просмотреть таблицу";
            this.просмотретьТаблицуToolStripMenuItem.Click += new System.EventHandler(this.ПросмотретьТаблицуToolStripMenuItem_Click);
            // 
            // FuelTableCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 285);
            this.Controls.Add(this.ButtonOut);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CalibrationBox);
            this.Controls.Add(this.FuelBox);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FuelTableCalibration";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Топливо и градуировочные таблицы";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FuelTableCalibration_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox FuelBox;
        private System.Windows.Forms.ListBox CalibrationBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сопоставитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem топливоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem градуировочнуюТаблицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem всёToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переименоватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem топливоToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem градуировочнуюТаблицуToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem топливоИТаблицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem просмотретьТаблицуToolStripMenuItem;
    }
}