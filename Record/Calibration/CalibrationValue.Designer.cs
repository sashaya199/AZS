﻿namespace Record.Calibration
{
    partial class CalibrationValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibrationValue));
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.MinMM = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MaxMM = new System.Windows.Forms.TextBox();
            this.Step = new System.Windows.Forms.TextBox();
            this.FuelComboBox = new System.Windows.Forms.ComboBox();
            this.TypeStep = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(286, 134);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 20;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Заполнить";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(286, 84);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(104, 40);
            this.ButtonIn.TabIndex = 21;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 19);
            this.label1.TabIndex = 22;
            this.label1.Text = "Минимальные мм";
            // 
            // MinMM
            // 
            this.MinMM.Location = new System.Drawing.Point(156, 10);
            this.MinMM.Name = "MinMM";
            this.MinMM.Size = new System.Drawing.Size(123, 27);
            this.MinMM.TabIndex = 23;
            this.MinMM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MinMM_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 19);
            this.label2.TabIndex = 24;
            this.label2.Text = "Максимальные мм";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 19);
            this.label3.TabIndex = 25;
            this.label3.Text = "Шаг";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 19);
            this.label4.TabIndex = 26;
            this.label4.Text = "Топливо";
            // 
            // MaxMM
            // 
            this.MaxMM.Location = new System.Drawing.Point(156, 43);
            this.MaxMM.Name = "MaxMM";
            this.MaxMM.Size = new System.Drawing.Size(123, 27);
            this.MaxMM.TabIndex = 23;
            this.MaxMM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MinMM_KeyPress);
            // 
            // Step
            // 
            this.Step.Location = new System.Drawing.Point(156, 76);
            this.Step.Name = "Step";
            this.Step.Size = new System.Drawing.Size(123, 27);
            this.Step.TabIndex = 23;
            this.Step.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MinMM_KeyPress);
            // 
            // FuelComboBox
            // 
            this.FuelComboBox.FormattingEnabled = true;
            this.FuelComboBox.Location = new System.Drawing.Point(156, 142);
            this.FuelComboBox.Name = "FuelComboBox";
            this.FuelComboBox.Size = new System.Drawing.Size(123, 27);
            this.FuelComboBox.TabIndex = 27;
            this.FuelComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FuelComboBox_KeyPress);
            // 
            // TypeStep
            // 
            this.TypeStep.DisplayMember = "0";
            this.TypeStep.FormattingEnabled = true;
            this.TypeStep.Items.AddRange(new object[] {
            "Литры",
            "Миллиметры"});
            this.TypeStep.Location = new System.Drawing.Point(156, 109);
            this.TypeStep.Name = "TypeStep";
            this.TypeStep.Size = new System.Drawing.Size(123, 27);
            this.TypeStep.TabIndex = 27;
            this.TypeStep.Tag = "";
            this.TypeStep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FuelComboBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 19);
            this.label5.TabIndex = 26;
            this.label5.Text = "Тип шага";
            // 
            // CalibrationValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 188);
            this.Controls.Add(this.TypeStep);
            this.Controls.Add(this.FuelComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Step);
            this.Controls.Add(this.MaxMM);
            this.Controls.Add(this.MinMM);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonIn);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CalibrationValue";
            this.ShowIcon = false;
            this.Text = "Градуировка";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Calibration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MinMM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox MaxMM;
        private System.Windows.Forms.TextBox Step;
        private System.Windows.Forms.ComboBox FuelComboBox;
        private System.Windows.Forms.ComboBox TypeStep;
        private System.Windows.Forms.Label label5;
    }
}