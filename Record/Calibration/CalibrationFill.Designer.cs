﻿namespace Record.Calibration
{
    partial class CalibrationFill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibrationFill));
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonDiaplFile = new Bunifu.Framework.UI.BunifuThinButton2();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Заполнить";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(523, 239);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(104, 40);
            this.ButtonIn.TabIndex = 22;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(523, 289);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 23;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ButtonDiaplFile
            // 
            this.ButtonDiaplFile.ActiveBorderThickness = 1;
            this.ButtonDiaplFile.ActiveCornerRadius = 20;
            this.ButtonDiaplFile.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonDiaplFile.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonDiaplFile.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonDiaplFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonDiaplFile.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonDiaplFile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonDiaplFile.BackgroundImage")));
            this.ButtonDiaplFile.ButtonText = "Выбрать фаил";
            this.ButtonDiaplFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDiaplFile.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDiaplFile.ForeColor = System.Drawing.Color.Black;
            this.ButtonDiaplFile.IdleBorderThickness = 1;
            this.ButtonDiaplFile.IdleCornerRadius = 20;
            this.ButtonDiaplFile.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDiaplFile.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonDiaplFile.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDiaplFile.Location = new System.Drawing.Point(523, 175);
            this.ButtonDiaplFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonDiaplFile.Name = "ButtonDiaplFile";
            this.ButtonDiaplFile.Size = new System.Drawing.Size(104, 54);
            this.ButtonDiaplFile.TabIndex = 22;
            this.ButtonDiaplFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonDiaplFile.Click += new System.EventHandler(this.ButtonDiaplFile_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Text|*.txt";
            // 
            // CalibrationFill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(640, 343);
            this.Controls.Add(this.ButtonOut);
            this.Controls.Add(this.ButtonDiaplFile);
            this.Controls.Add(this.ButtonIn);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CalibrationFill";
            this.Text = "Заполнение градуировочной таблицы";
            this.Load += new System.EventHandler(this.CalibrationFill_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonDiaplFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}