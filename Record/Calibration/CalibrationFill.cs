﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using ClassAll.Queries;
using System.Windows.Forms;
using System.IO;

namespace Record.Calibration
{
    public partial class CalibrationFill : Form
    {
        public CalibrationFill(string minMM, string maxMM, string step, string fuel, string typeStep)
        {
            InitializeComponent();
            this.minMM = Convert.ToInt32(minMM);
            this.maxMM = Convert.ToInt32(maxMM);
            this.step = Convert.ToInt32(step);
            this.fuel = fuel;
            this.typeStep = typeStep;
        }
        private int minMM;
        private int maxMM;
        private int step;
        private string typeStep;
        private string fuel;
        private List<int> arryValue;
        private List<Label> labelList;
        private List<TextBox> textBoxList;

        private void CalibrationFill_Load(object sender, EventArgs e)
        {
            labelList = new List<Label>();
            textBoxList = new List<TextBox>();
            arryValue = new List<int>();
            int steps = (maxMM - minMM) / step;
            for (int i = 0; i < steps + 1; i++)
            {
                AddControl(i);
            }
        }
        private void AddControl(int i)
        {
            NewLabel(i);
            NewTextBox(i);
            this.Controls.Add(labelList[i]);
            this.Controls.Add(textBoxList[i]);

        }

        private void NewLabel(int i)
        {
            arryValue.Add(minMM + i * step);
            labelList.Add(new Label());
            if (typeStep == "Литры")
            {
                labelList[i].Text = $"Введите миллиметры  на {arryValue[i]} литров";
            }
            else
            {
                labelList[i].Text = $"Введите литры  на {arryValue[i]} мм";
            }
            labelList[i].Location = new Point(10, 30 * i);
            labelList[i].AutoSize = true;
        }

        private void NewTextBox(int i)
        {
            textBoxList.Add(new TextBox());
            textBoxList[i].Location = new Point(330, 30 * i);
            textBoxList[i].Click += CalibrationFill_Click; ;
            textBoxList[i].KeyPress += CalibrationFill_KeyPress;
        }

        private void CalibrationFill_Click(object sender, EventArgs e)
        {
            foreach (var item in textBoxList)
            {
                if (item.Focused == true)
                {
                    item.BackColor = Color.Empty;
                }
            }
        }

        private void CalibrationFill_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonIn_Click(object sender, EventArgs e)
        {
            if (СorrectnessData())
            {
                List<int> fullMM = new List<int>();
                List<int> fullLiters = new List<int>();
                if (typeStep == "Литры")
                {
                    FullCalibrationLiters(fullMM, fullLiters);
                }
                else
                {
                    FullCalibrationMM(fullMM, fullLiters);
                }
                AddDB(fullMM, fullLiters);
                MessageBox.Show("Успешно добавленно");
            }
        }

        private bool СorrectnessData()// проверка на введёные значения что бы не уменьшались
        {
            bool fillIn = true;
            for (int i = 1; i < textBoxList.Count; i++)
            {
                if (String.IsNullOrEmpty(textBoxList[i - 1].Text) && String.IsNullOrEmpty(textBoxList[i].Text))
                {
                    MessageBox.Show("");
                    fillIn = false;
                    break;
                }
                else
                {
                    if (Convert.ToInt32(textBoxList[i - 1].Text) > Convert.ToInt32(textBoxList[i].Text))
                    {
                        fillIn = false;
                        MessageBox.Show("Введены не правленые данные");
                        textBoxList[i - 1].Focus();
                        textBoxList[i - 1].BackColor = Color.FromArgb(255, 0, 255, 0);
                        textBoxList[i].BackColor = Color.FromArgb(255, 200, 0, 0);
                        break;
                    }
                }
            }

            return fillIn;
        }

        private void AddDB(List<int> fullMM, List<int> fullLiters)// дабавления данных в базу
        {
            using (InquiryInto inquiryInto = new InquiryInto())
            {
                for (int i = 0; i < fullMM.Count; i++)
                {
                    inquiryInto.CalibrationAdd(fullMM[i].ToString(), fullLiters[i].ToString(), fuel);
                }
            }
        }

        private void FullCalibrationMM(List<int> fullMM, List<int> fullLiters) // точный расчёт по мм
        {
            for (int i = 1; i < arryValue.Count; i++)
            {
                int oneValueStep = (Convert.ToInt32(textBoxList[i].Text) - Convert.ToInt32(textBoxList[i - 1].Text)) / step;
                int startValueStepLiters = Convert.ToInt32(textBoxList[i - 1].Text);
                int startValueStepMM = arryValue[i - 1];
                for (int j = 0; j < step; j++)
                {
                    fullLiters.Add(startValueStepLiters);
                    startValueStepLiters += oneValueStep;
                    fullMM.Add(startValueStepMM);
                    startValueStepMM += 1;
                }
                if (i == arryValue.Count - 1)
                {
                    fullMM.Add(arryValue[i]);
                    fullLiters.Add(Convert.ToInt32(textBoxList[i].Text));
                }
            }
        }
        private void FullCalibrationLiters(List<int> fullMM, List<int> fullLiters) // точный расчёт по литрам
        {
            for (int i = 1; i < arryValue.Count; i++)
            {
                int steps = Convert.ToInt32(textBoxList[i].Text) - Convert.ToInt32(textBoxList[i - 1].Text);
                int oneStep = step / steps;
                int startValueStepLiters = arryValue[i - 1];
                int startValueStepMM = Convert.ToInt32(textBoxList[i - 1].Text);
                for (int j = 0; j < steps; j++)
                {
                    fullLiters.Add(startValueStepLiters);
                    startValueStepLiters += oneStep;
                    fullMM.Add(startValueStepMM);
                    startValueStepMM += 1;

                }
                if (i == arryValue.Count - 1)
                {
                    fullMM.Add(Convert.ToInt32(textBoxList[i].Text));
                    fullLiters.Add(arryValue[i]);
                }
            }
        }

        private void ButtonDiaplFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            List<string> fileValue = new List<string>();
            using (StreamReader read = new StreamReader(openFileDialog.FileName, Encoding.Default)) // чтение нстроек
            {
                string line;
                while ((line = read.ReadLine()) != null)
                {
                    fileValue.Add(line);
                }
            }
            if (fileValue.Count == textBoxList.Count)
            {
                NewMethod(fileValue);
            }
            else if (fileValue.Count > textBoxList.Count)
            {
                if (MessageBox.Show("","",MessageBoxButtons.YesNo)==DialogResult.Yes)
                {
                    NewMethod(fileValue);

                }
            }
            else if (fileValue.Count < textBoxList.Count)
            {
                if (MessageBox.Show("", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    NewMethod(fileValue);
                }
            }
        }

        private void NewMethod(List<string> fileValue)
        {
            for (int i = 0; i < textBoxList.Count; i++)
            {
                textBoxList[i].Text = fileValue[i];
            }
        }
    }
}

