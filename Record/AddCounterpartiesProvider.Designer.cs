﻿namespace Record
{
    partial class AddCounterpartiesProvider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCounterpartiesProvider));
            this.DataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Наименование = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DdIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonEdit = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonDel = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxName = new System.Windows.Forms.TextBox();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGrid.BackgroundColor = System.Drawing.Color.DarkGray;
            this.DataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Наименование});
            this.DataGrid.DoubleBuffered = true;
            this.DataGrid.EnableHeadersVisualStyles = false;
            this.DataGrid.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.DataGrid.HeaderForeColor = System.Drawing.Color.Black;
            this.DataGrid.Location = new System.Drawing.Point(16, 95);
            this.DataGrid.Name = "DataGrid";
            this.DataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGrid.Size = new System.Drawing.Size(519, 420);
            this.DataGrid.TabIndex = 11;
            this.DataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_CellContentClick);
            // 
            // Наименование
            // 
            this.Наименование.HeaderText = "Наименование";
            this.Наименование.Name = "Наименование";
            // 
            // DdIn
            // 
            this.DdIn.ActiveBorderThickness = 1;
            this.DdIn.ActiveCornerRadius = 20;
            this.DdIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.DdIn.ActiveForecolor = System.Drawing.Color.Black;
            this.DdIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.DdIn.BackColor = System.Drawing.SystemColors.Control;
            this.DdIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("DdIn.BackgroundImage")));
            this.DdIn.ButtonText = "Ввести";
            this.DdIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DdIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DdIn.ForeColor = System.Drawing.Color.Black;
            this.DdIn.IdleBorderThickness = 1;
            this.DdIn.IdleCornerRadius = 20;
            this.DdIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DdIn.IdleForecolor = System.Drawing.Color.Black;
            this.DdIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DdIn.Location = new System.Drawing.Point(16, 47);
            this.DdIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DdIn.Name = "DdIn";
            this.DdIn.Size = new System.Drawing.Size(104, 40);
            this.DdIn.TabIndex = 8;
            this.DdIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DdIn.Click += new System.EventHandler(this.DdIn_Click);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.ActiveBorderThickness = 1;
            this.ButtonEdit.ActiveCornerRadius = 20;
            this.ButtonEdit.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonEdit.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonEdit.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonEdit.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonEdit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonEdit.BackgroundImage")));
            this.ButtonEdit.ButtonText = "Изменить запись";
            this.ButtonEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonEdit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonEdit.ForeColor = System.Drawing.Color.Black;
            this.ButtonEdit.IdleBorderThickness = 1;
            this.ButtonEdit.IdleCornerRadius = 20;
            this.ButtonEdit.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonEdit.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonEdit.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonEdit.Location = new System.Drawing.Point(128, 47);
            this.ButtonEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.Size = new System.Drawing.Size(179, 40);
            this.ButtonEdit.TabIndex = 9;
            this.ButtonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonDel
            // 
            this.ButtonDel.ActiveBorderThickness = 1;
            this.ButtonDel.ActiveCornerRadius = 20;
            this.ButtonDel.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonDel.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonDel.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonDel.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonDel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonDel.BackgroundImage")));
            this.ButtonDel.ButtonText = "Удалить";
            this.ButtonDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDel.ForeColor = System.Drawing.Color.Black;
            this.ButtonDel.IdleBorderThickness = 1;
            this.ButtonDel.IdleCornerRadius = 20;
            this.ButtonDel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonDel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.Location = new System.Drawing.Point(315, 47);
            this.ButtonDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonDel.Name = "ButtonDel";
            this.ButtonDel.Size = new System.Drawing.Size(104, 40);
            this.ButtonDel.TabIndex = 10;
            this.ButtonDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "Наименование:";
            // 
            // TextBoxName
            // 
            this.TextBoxName.Location = new System.Drawing.Point(139, 6);
            this.TextBoxName.Name = "TextBoxName";
            this.TextBoxName.Size = new System.Drawing.Size(280, 27);
            this.TextBoxName.TabIndex = 13;
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(427, 47);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 12;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // AddCounterpartiesProvider
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 537);
            this.Controls.Add(this.DataGrid);
            this.Controls.Add(this.DdIn);
            this.Controls.Add(this.ButtonEdit);
            this.Controls.Add(this.ButtonDel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxName);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddCounterpartiesProvider";
            this.ShowIcon = false;
            this.Text = "AddCounterpartiesProvider";
            this.Load += new System.EventHandler(this.AddCounterpartiesProvider_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomDataGrid DataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Наименование;
        private Bunifu.Framework.UI.BunifuThinButton2 DdIn;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonEdit;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonDel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxName;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
    }
}