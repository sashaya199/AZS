﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using ClassAll;
using System.Text;
using System.Windows.Forms;

namespace Record
{
    public partial class RecordEdit : Form
    {
        RenevalUpdate renevalUpdate = new RenevalUpdate();
        RenewalMeaning renewalMeaning = new RenewalMeaning();
        public RecordEdit(string ID, string variableRecord)
        {
            InitializeComponent();
            inventoriId = ID;
            this.variableRecord = variableRecord;
        }
        private string inventoriId;
        private string variableRecord;
        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RecordEdit_Load(object sender, EventArgs e)
        {
            Updata();
        }

        private void Updata()
        {
            ViewFuel.Items.AddRange(renewalMeaning.AllFuel());
            ViewCounterparties.Items.AddRange(renewalMeaning.Counterparties());

        }

        private void LitersBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }
    }
}
