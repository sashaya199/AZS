﻿namespace Record
{
    partial class RecordEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecordEdit));
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Date = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.ViewFuel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ViewCounterparties = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LitersBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(209, 155);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 1;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Изменить";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(13, 155);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(104, 40);
            this.ButtonIn.TabIndex = 1;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Location = new System.Drawing.Point(5, 84);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(50, 19);
            this.Date.TabIndex = 19;
            this.Date.Text = "Дата:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(112, 78);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(193, 27);
            this.dateTimePicker1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 19);
            this.label1.TabIndex = 20;
            this.label1.Text = "Вид топлива:";
            // 
            // ViewFuel
            // 
            this.ViewFuel.DisplayMember = "0";
            this.ViewFuel.FormattingEnabled = true;
            this.ViewFuel.Location = new System.Drawing.Point(112, 12);
            this.ViewFuel.Name = "ViewFuel";
            this.ViewFuel.Size = new System.Drawing.Size(193, 27);
            this.ViewFuel.TabIndex = 17;
            this.ViewFuel.Tag = "0";
            this.ViewFuel.ValueMember = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 19);
            this.label2.TabIndex = 21;
            this.label2.Text = "Контрагент:";
            // 
            // ViewCounterparties
            // 
            this.ViewCounterparties.DisplayMember = "0";
            this.ViewCounterparties.FormattingEnabled = true;
            this.ViewCounterparties.Location = new System.Drawing.Point(111, 45);
            this.ViewCounterparties.Name = "ViewCounterparties";
            this.ViewCounterparties.Size = new System.Drawing.Size(193, 27);
            this.ViewCounterparties.TabIndex = 18;
            this.ViewCounterparties.Tag = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 19);
            this.label3.TabIndex = 22;
            this.label3.Text = "Литры:";
            // 
            // LitersBox
            // 
            this.LitersBox.Location = new System.Drawing.Point(111, 111);
            this.LitersBox.Name = "LitersBox";
            this.LitersBox.Size = new System.Drawing.Size(193, 27);
            this.LitersBox.TabIndex = 23;
            this.LitersBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LitersBox_KeyPress);
            // 
            // RecordEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 209);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ViewFuel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ViewCounterparties);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LitersBox);
            this.Controls.Add(this.ButtonIn);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RecordEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Изменение записи";
            this.Load += new System.EventHandler(this.RecordEdit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ViewFuel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ViewCounterparties;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox LitersBox;
    }
}