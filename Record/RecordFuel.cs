﻿using ClassAll.Queries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Record
{
    public partial class RecordFuel : DockContent
    {
        public RecordFuel()
        {
            InitializeComponent();
        }
        private string code = "";

        private string Code { get => code; set => code = value; }

        private void ЗакрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void UpdataProgressBar()// обновление прогресс баров и данных
        {
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                string[] typeFuel = renewal.AllFuel(); // получение списака топлива
                foreach (string item in typeFuel)// рапонение прогрессбаров
                {
                    UpdataProgessBars(item, renewal);
                }
                renewal.CloseOpeBD();
            }
        }

        private void UpdataProgessBars(string item, InquiryMeaning renewal) // Выполнение запросов для получения данных
        {
            string max = renewal.SingleValueColum("select max(Литры) as Литры from " + item, "Литры");
            renewal.Data.Tables.Clear();
            int valueFuel = renewal.RemainderFuel(item);//выполнения запроса на остаток 
            int fillPercentage = 0;
            if (valueFuel > 0)
            {
                fillPercentage = (valueFuel * 100 / Convert.ToInt32(max));
            }
            ProgressBarsFilling(item, fillPercentage, valueFuel);
        }

        private void ProgressBarsFilling(string Fuel, int fillPercentage, int valueFuel)// заполнение прогресс баров
            // переделать по динамическое заполнение
        {
            if (fillPercentage > 100)
            {
                fillPercentage = 100;
            }
            switch (Fuel)
            {
                case "ДТ":
                    DtProgressBar.Value = fillPercentage;
                    LabelDt.Text = $" {Fuel}: {valueFuel} л.";
                    break;
                case "СМТ":
                    SmtProgressBar.Value = fillPercentage;
                    LabelSmt.Text = $" {Fuel}: {valueFuel} л.";
                    break;
                case "Р92":
                    Ai92ProgressBar.Value = fillPercentage;
                    LabelAi92.Text = $" {Fuel}: {valueFuel} л.";
                    break;
                case "Н80":
                    Ai80ProgressBar.Value = fillPercentage;
                    LabelAi80.Text = $" {Fuel}: {valueFuel} л.";
                    break;
                default:
                    break;
            }
        }

        private void VariableVeiw()
        {
            ViewCounterparties.Items.Clear();
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                if (Coming.Checked == true)
                {
                    ViewCounterparties.Items.AddRange(renewal.Provider());
                    renewal.ComingFilling();
                    DataGrid.DataSource = renewal.DataTable;
                }
                else if (Consumption.Checked == true)
                {
                    ViewCounterparties.Items.AddRange(renewal.Counterparties());
                    renewal.ConsumptionFilling();
                    DataGrid.DataSource = renewal.DataTable;
                }
            }
            ViewCounterparties.SelectedIndex = 0;
        }
        private void UpDataControl()
        {
            using (InquiryMeaning renewal = new InquiryMeaning())
            {
                ViewFuel.Items.AddRange(renewal.AllFuel());
            }
            VariableVeiw();
            ViewFuel.SelectedIndex = 0;
        }

        private void DdIn_Click(object sender, EventArgs e)// переделать
        {
            using (InquiryInto renewalInto = new InquiryInto())
            {
                if (Coming.Checked == true)
                {
                    renewalInto.RegistrationComing(dateTimePicker1.Value.Date.ToString(), ViewFuel.Text, ViewCounterparties.Text, LitersBox.Text);
                    UpdataProgressBar();
                    VariableVeiw();
                }
                else if (Consumption.Checked == true)
                {
                    renewalInto.RegistrationConsumption(dateTimePicker1.Value.Date.ToString(), ViewFuel.Text, ViewCounterparties.Text, LitersBox.Text);
                    UpdataProgressBar();
                    VariableVeiw();
                }
            }
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Желаете удалить запись?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int id = DataGrid.CurrentRow.Index;
                DataGrid.Rows.RemoveAt(id);
                using (InquiryDeleted inquiryDeleted = new InquiryDeleted())
                {
                    if (Code != String.Empty)
                    {
                        if (Coming.Checked == true)
                        {
                            inquiryDeleted.DeleteRecordComing(Code);
                        }
                        else if (Consumption.Checked == true)
                        {
                            inquiryDeleted.DeleteRecordConsumption(Code);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для изменения");
                    }
                }
                bool test = new TestInquiry().Test();
                if (test == false)
                {
                    Dispose();
                }
                UpdataProgressBar();
            }
        }

        private void ButtonEdit_Click(object sender, EventArgs e)//запуск формы для редоктирования 
        {
            if (MessageBox.Show("Желаете изменить запись?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (Code != String.Empty)
                {
                    using (RenevalInquiry renevalInquiry = new RenevalInquiry())
                    {
                        if (Coming.Checked == true)
                        {
                            renevalInquiry.EditRecordComing(Code, dateTimePicker1.Value.Date.ToString("MM-dd-yyyy"), ViewFuel.Text, ViewCounterparties.Text, LitersBox.Text);
                            UpdataProgressBar();
                            VariableVeiw();
                        }
                        else if (Consumption.Checked == true)
                        {
                            renevalInquiry.EditRecordConsumption(Code, dateTimePicker1.Value.Date.ToString("MM-dd-yyyy"), ViewFuel.Text, ViewCounterparties.Text, LitersBox.Text);
                            UpdataProgressBar();
                            VariableVeiw();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Не выбрана строка для изменения");
                }
            }

        }

        private void LitersBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void DataGrid_Click(object sender, EventArgs e)
        {
            Code = DataGrid.CurrentRow.Cells["Код"].Value.ToString();
            string counterag = "";
            if (DataGrid.CurrentRow.Cells["Дата"].Value.ToString() != String.Empty)
            {
                dateTimePicker1.Value = Convert.ToDateTime(DataGrid.CurrentRow.Cells["Дата"].Value);
            }
            LitersBox.Text = DataGrid.CurrentRow.Cells["Литры"].Value.ToString();
            for (int i = 0; i < ViewFuel.Items.Count; i++)
            {
                if (ViewFuel.Items[i].ToString() == DataGrid.CurrentRow.Cells["Топливо"].Value.ToString())
                {
                    ViewFuel.SelectedIndex = i;
                    break;
                }
            }
            if (Coming.Checked == true)
            {
                counterag = DataGrid.CurrentRow.Cells["Поставщик"].Value.ToString();
            }
            else if (Consumption.Checked == true)
            {
                counterag = DataGrid.CurrentRow.Cells["Контрагент"].Value.ToString();
            }
            for (int i = 0; i < ViewCounterparties.Items.Count; i++)
            {
                if (ViewCounterparties.Items[i].ToString() == counterag)
                {
                    ViewCounterparties.SelectedIndex = i;
                    break;
                }
            }
        }

        private void ОбновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpDataControl();
            UpdataProgressBar();
            dateTimePicker1.Value = DateTime.Today;
        }

        private void Coming_Click(object sender, EventArgs e)
        {
            VariableVeiw();
        }

        private void Records_Load(object sender, EventArgs e)
        {
            UpDataControl();
            UpdataProgressBar();
            dateTimePicker1.Value = DateTime.Today;
        }
    }
}
