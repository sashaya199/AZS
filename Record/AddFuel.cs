﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Record.Calibration;
using ClassAll.Queries;
using System.Windows.Forms;

namespace Record
{
    public partial class AddFuel : Form
    {
        public AddFuel()
        {
            InitializeComponent();
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void ButtonIn_Click(object sender, EventArgs e)
        {
            using (InquiryInto inquiryInto = new InquiryInto())
            {
                inquiryInto.CreadFuel(NewFuelTextBox.Text);
            }

            if (MessageBox.Show($"Для использования топлива: {NewFuelTextBox.Text}. Необходимо заполнить градуировочную таблицу. Желаете заполнить? ", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Dispose();
                new CalibrationValue().Show();
            }
            else
            {
                this.Dispose();
            }
        }

        private void AddFuel_MouseMove(object sender, MouseEventArgs e)
        {
            this.TopMost = false;
        }
    }
}
