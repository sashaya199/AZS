﻿using ClassAll.Queries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Record
{
    public partial class AddCounterpartiesProvider : Form
    {
        public AddCounterpartiesProvider(string type)
        {
            InitializeComponent();
            this.type = type;
            Text = type;
        }
        private string type;
        private string nameCounterparties = "";


        private void UpDataGrid()
        {
            DataGrid.Rows.Clear();
            InquiryMeaning inquiryMeaning = new InquiryMeaning();
            string[] counter;
            if (type == "Поставщики")
            {
                counter = inquiryMeaning.Provider();
            }
            else
            {
                counter = inquiryMeaning.Counterparties();
            }
            foreach (var item in counter)
            {
                DataGrid.Rows.Add(item);
            }
        }

        private void DataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGrid.CurrentRow.Cells["Наименование"].Value != null)
            {
                nameCounterparties = DataGrid.CurrentRow.Cells[0].Value.ToString();
            }
            else // вылетает исключение при Cells = null
            {
                nameCounterparties = "";
            }
            TextBoxName.Text = nameCounterparties;
        }

        private void DdIn_Click(object sender, EventArgs e)
        {
            
            string[] listcounterarties;
            using (InquiryMeaning inquiryMeaning = new InquiryMeaning())
            {
                if (type == "Поставщики")
                {
                    listcounterarties = inquiryMeaning.Provider();
                }
                else
                {
                    listcounterarties = inquiryMeaning.Counterparties();
                }
            }
            bool check = true;
            foreach (var item in listcounterarties)
            {
                if (item == TextBoxName.Text)
                {
                    MessageBox.Show("Данный контрагент существует");
                    check = false;
                    break;
                }
            }
            if (check == true)
            {
                using (InquiryInto inquiryInto = new InquiryInto())
                {
                    if (type == "Поставщики")
                    {
                        inquiryInto.Provider(TextBoxName.Text);
                    }
                    else
                    {
                        inquiryInto.Counterparties(TextBoxName.Text);
                    }
                }
                UpDataGrid();
            }
            TextBoxName.Text = "";
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Желаете удалаить запись {nameCounterparties}?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int id = DataGrid.CurrentRow.Index;
                if (DataGrid.Rows.Count - 1 != id)
                {
                    DataGrid.Rows.RemoveAt(id);
                    using (InquiryDeleted inquiryDeleted = new InquiryDeleted())
                    {
                        if (type == "Поставщики")
                        {
                            inquiryDeleted.DeleteProvider(nameCounterparties);
                        }
                        else
                        {
                            inquiryDeleted.DeleteCounterparties(nameCounterparties);
                        }
                    }
                    TextBoxName.Text = "";
                }
            }
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Желаете изменить запись с {nameCounterparties} на {TextBoxName.Text}", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (nameCounterparties != String.Empty | nameCounterparties != null)
                {
                    using (RenevalInquiry renevalInquiry = new RenevalInquiry())
                    {
                        if (type == "Поставщики")
                        {
                            renevalInquiry.EditProvider(TextBoxName.Text, nameCounterparties);
                        }
                        else
                        {
                            renevalInquiry.EditCunterparties(TextBoxName.Text, nameCounterparties);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Не выбрана запись для изменения");
                }
            }
            TextBoxName.Text = "";
            UpDataGrid();
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void AddCounterpartiesProvider_Load(object sender, EventArgs e)
        {
            UpDataGrid();
        }
    }
}
