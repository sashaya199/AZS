﻿namespace Record
{
    partial class AddFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddFuel));
            this.label1 = new System.Windows.Forms.Label();
            this.NewFuelTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ButtonIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonClose = new Bunifu.Framework.UI.BunifuThinButton2();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите название топлива";
            // 
            // NewFuelTextBox
            // 
            this.NewFuelTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewFuelTextBox.Location = new System.Drawing.Point(3, 22);
            this.NewFuelTextBox.Name = "NewFuelTextBox";
            this.NewFuelTextBox.Size = new System.Drawing.Size(257, 27);
            this.NewFuelTextBox.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.NewFuelTextBox);
            this.flowLayoutPanel1.Controls.Add(this.ButtonIn);
            this.flowLayoutPanel1.Controls.Add(this.ButtonClose);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(265, 97);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // ButtonIn
            // 
            this.ButtonIn.ActiveBorderThickness = 1;
            this.ButtonIn.ActiveCornerRadius = 20;
            this.ButtonIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonIn.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonIn.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonIn.BackgroundImage")));
            this.ButtonIn.ButtonText = "Вывод данных";
            this.ButtonIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonIn.ForeColor = System.Drawing.Color.Black;
            this.ButtonIn.IdleBorderThickness = 1;
            this.ButtonIn.IdleCornerRadius = 20;
            this.ButtonIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonIn.Location = new System.Drawing.Point(4, 57);
            this.ButtonIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(131, 40);
            this.ButtonIn.TabIndex = 23;
            this.ButtonIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // ButtonClose
            // 
            this.ButtonClose.ActiveBorderThickness = 1;
            this.ButtonClose.ActiveCornerRadius = 20;
            this.ButtonClose.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonClose.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonClose.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonClose.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonClose.BackgroundImage")));
            this.ButtonClose.ButtonText = "Отмена";
            this.ButtonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonClose.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonClose.ForeColor = System.Drawing.Color.Black;
            this.ButtonClose.IdleBorderThickness = 1;
            this.ButtonClose.IdleCornerRadius = 20;
            this.ButtonClose.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonClose.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonClose.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonClose.Location = new System.Drawing.Point(143, 57);
            this.ButtonClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(116, 40);
            this.ButtonClose.TabIndex = 24;
            this.ButtonClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // AddFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 115);
            this.ControlBox = false;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddFuel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление вида топлива";
            this.TopMost = true;
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AddFuel_MouseMove);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NewFuelTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonIn;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonClose;
    }
}