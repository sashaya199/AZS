﻿namespace Record
{
    partial class RecordFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecordFuel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Ai92ProgressBar = new Bunifu.Framework.UI.BunifuGauge();
            this.SmtProgressBar = new Bunifu.Framework.UI.BunifuGauge();
            this.DtProgressBar = new Bunifu.Framework.UI.BunifuGauge();
            this.Ai80ProgressBar = new Bunifu.Framework.UI.BunifuGauge();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Coming = new System.Windows.Forms.RadioButton();
            this.Consumption = new System.Windows.Forms.RadioButton();
            this.MenuStripBar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Date = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ViewFuel = new System.Windows.Forms.ComboBox();
            this.LabelAi80 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelAi92 = new System.Windows.Forms.Label();
            this.ViewCounterparties = new System.Windows.Forms.ComboBox();
            this.LabelDt = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelSmt = new System.Windows.Forms.Label();
            this.LitersBox = new System.Windows.Forms.TextBox();
            this.DataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.DdIn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonEdit = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ThinButtonRel = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonDel = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.groupBox1.SuspendLayout();
            this.MenuStripBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // Ai92ProgressBar
            // 
            this.Ai92ProgressBar.AutoScroll = true;
            this.Ai92ProgressBar.AutoSize = true;
            this.Ai92ProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.Ai92ProgressBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Ai92ProgressBar.BackgroundImage")));
            this.Ai92ProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ai92ProgressBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Ai92ProgressBar.Location = new System.Drawing.Point(297, 14);
            this.Ai92ProgressBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Ai92ProgressBar.Name = "Ai92ProgressBar";
            this.Ai92ProgressBar.ProgressBgColor = System.Drawing.Color.Gray;
            this.Ai92ProgressBar.ProgressColor1 = System.Drawing.Color.Lime;
            this.Ai92ProgressBar.ProgressColor2 = System.Drawing.Color.Tomato;
            this.Ai92ProgressBar.Size = new System.Drawing.Size(136, 95);
            this.Ai92ProgressBar.TabIndex = 41;
            this.Ai92ProgressBar.Thickness = 10;
            this.Ai92ProgressBar.Value = 50;
            // 
            // SmtProgressBar
            // 
            this.SmtProgressBar.AutoScroll = true;
            this.SmtProgressBar.AutoSize = true;
            this.SmtProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.SmtProgressBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SmtProgressBar.BackgroundImage")));
            this.SmtProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.SmtProgressBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SmtProgressBar.Location = new System.Drawing.Point(155, 14);
            this.SmtProgressBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SmtProgressBar.Name = "SmtProgressBar";
            this.SmtProgressBar.ProgressBgColor = System.Drawing.Color.Gray;
            this.SmtProgressBar.ProgressColor1 = System.Drawing.Color.Lime;
            this.SmtProgressBar.ProgressColor2 = System.Drawing.Color.Tomato;
            this.SmtProgressBar.Size = new System.Drawing.Size(136, 95);
            this.SmtProgressBar.TabIndex = 40;
            this.SmtProgressBar.Thickness = 10;
            this.SmtProgressBar.Value = 50;
            // 
            // DtProgressBar
            // 
            this.DtProgressBar.AutoScroll = true;
            this.DtProgressBar.AutoSize = true;
            this.DtProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.DtProgressBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("DtProgressBar.BackgroundImage")));
            this.DtProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.DtProgressBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DtProgressBar.Location = new System.Drawing.Point(10, 14);
            this.DtProgressBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DtProgressBar.Name = "DtProgressBar";
            this.DtProgressBar.ProgressBgColor = System.Drawing.Color.Gray;
            this.DtProgressBar.ProgressColor1 = System.Drawing.Color.Lime;
            this.DtProgressBar.ProgressColor2 = System.Drawing.Color.Tomato;
            this.DtProgressBar.Size = new System.Drawing.Size(136, 95);
            this.DtProgressBar.TabIndex = 39;
            this.DtProgressBar.Thickness = 10;
            this.DtProgressBar.Value = 50;
            // 
            // Ai80ProgressBar
            // 
            this.Ai80ProgressBar.AutoScroll = true;
            this.Ai80ProgressBar.AutoSize = true;
            this.Ai80ProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.Ai80ProgressBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Ai80ProgressBar.BackgroundImage")));
            this.Ai80ProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Ai80ProgressBar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Ai80ProgressBar.Location = new System.Drawing.Point(439, 14);
            this.Ai80ProgressBar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Ai80ProgressBar.Name = "Ai80ProgressBar";
            this.Ai80ProgressBar.ProgressBgColor = System.Drawing.Color.Gray;
            this.Ai80ProgressBar.ProgressColor1 = System.Drawing.Color.Lime;
            this.Ai80ProgressBar.ProgressColor2 = System.Drawing.Color.Tomato;
            this.Ai80ProgressBar.Size = new System.Drawing.Size(136, 95);
            this.Ai80ProgressBar.TabIndex = 38;
            this.Ai80ProgressBar.Thickness = 10;
            this.Ai80ProgressBar.Value = 50;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Location = new System.Drawing.Point(379, 166);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 27);
            this.dateTimePicker1.TabIndex = 25;
            this.dateTimePicker1.Value = new System.DateTime(2018, 3, 3, 12, 8, 55, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Coming);
            this.groupBox1.Controls.Add(this.Consumption);
            this.groupBox1.Location = new System.Drawing.Point(593, 166);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 86);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Вид ";
            // 
            // Coming
            // 
            this.Coming.AutoSize = true;
            this.Coming.Location = new System.Drawing.Point(7, 57);
            this.Coming.Name = "Coming";
            this.Coming.Size = new System.Drawing.Size(80, 26);
            this.Coming.TabIndex = 1;
            this.Coming.Text = "Приход";
            this.Coming.UseCompatibleTextRendering = true;
            this.Coming.UseVisualStyleBackColor = true;
            this.Coming.Click += new System.EventHandler(this.Coming_Click);
            // 
            // Consumption
            // 
            this.Consumption.AutoSize = true;
            this.Consumption.Checked = true;
            this.Consumption.Location = new System.Drawing.Point(7, 27);
            this.Consumption.Name = "Consumption";
            this.Consumption.Size = new System.Drawing.Size(75, 26);
            this.Consumption.TabIndex = 0;
            this.Consumption.TabStop = true;
            this.Consumption.Text = "Расход";
            this.Consumption.UseCompatibleTextRendering = true;
            this.Consumption.UseVisualStyleBackColor = true;
            this.Consumption.Click += new System.EventHandler(this.Coming_Click);
            // 
            // MenuStripBar
            // 
            this.MenuStripBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьToolStripMenuItem});
            this.MenuStripBar.Name = "MenuStripBar";
            this.MenuStripBar.Size = new System.Drawing.Size(129, 26);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Location = new System.Drawing.Point(311, 166);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(50, 19);
            this.Date.TabIndex = 33;
            this.Date.Text = "Дата:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 19);
            this.label1.TabIndex = 32;
            this.label1.Text = "Вид топлива:";
            // 
            // ViewFuel
            // 
            this.ViewFuel.DisplayMember = "0";
            this.ViewFuel.FormattingEnabled = true;
            this.ViewFuel.Location = new System.Drawing.Point(112, 166);
            this.ViewFuel.Name = "ViewFuel";
            this.ViewFuel.Size = new System.Drawing.Size(193, 27);
            this.ViewFuel.TabIndex = 26;
            this.ViewFuel.Tag = "0";
            this.ViewFuel.ValueMember = "0";
            // 
            // LabelAi80
            // 
            this.LabelAi80.AutoSize = true;
            this.LabelAi80.Location = new System.Drawing.Point(439, 112);
            this.LabelAi80.Name = "LabelAi80";
            this.LabelAi80.Size = new System.Drawing.Size(53, 19);
            this.LabelAi80.TabIndex = 31;
            this.LabelAi80.Text = "Аи80:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 19);
            this.label2.TabIndex = 30;
            this.label2.Text = "Контрагент:";
            // 
            // LabelAi92
            // 
            this.LabelAi92.AutoSize = true;
            this.LabelAi92.Location = new System.Drawing.Point(293, 112);
            this.LabelAi92.Name = "LabelAi92";
            this.LabelAi92.Size = new System.Drawing.Size(53, 19);
            this.LabelAi92.TabIndex = 34;
            this.LabelAi92.Text = "Аи92:";
            // 
            // ViewCounterparties
            // 
            this.ViewCounterparties.DisplayMember = "0";
            this.ViewCounterparties.FormattingEnabled = true;
            this.ViewCounterparties.Location = new System.Drawing.Point(112, 209);
            this.ViewCounterparties.Name = "ViewCounterparties";
            this.ViewCounterparties.Size = new System.Drawing.Size(193, 27);
            this.ViewCounterparties.TabIndex = 27;
            this.ViewCounterparties.Tag = "0";
            // 
            // LabelDt
            // 
            this.LabelDt.AutoSize = true;
            this.LabelDt.Location = new System.Drawing.Point(10, 112);
            this.LabelDt.Name = "LabelDt";
            this.LabelDt.Size = new System.Drawing.Size(36, 19);
            this.LabelDt.TabIndex = 29;
            this.LabelDt.Text = "ДТ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(311, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 19);
            this.label3.TabIndex = 35;
            this.label3.Text = "Литры:";
            // 
            // LabelSmt
            // 
            this.LabelSmt.AutoSize = true;
            this.LabelSmt.Location = new System.Drawing.Point(151, 112);
            this.LabelSmt.Name = "LabelSmt";
            this.LabelSmt.Size = new System.Drawing.Size(47, 19);
            this.LabelSmt.TabIndex = 28;
            this.LabelSmt.Text = "СМТ:";
            // 
            // LitersBox
            // 
            this.LitersBox.Location = new System.Drawing.Point(379, 209);
            this.LitersBox.Name = "LitersBox";
            this.LitersBox.Size = new System.Drawing.Size(200, 27);
            this.LitersBox.TabIndex = 36;
            this.LitersBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LitersBox_KeyPress);
            // 
            // DataGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGrid.BackgroundColor = System.Drawing.Color.DarkGray;
            this.DataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGrid.DoubleBuffered = true;
            this.DataGrid.EnableHeadersVisualStyles = false;
            this.DataGrid.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.DataGrid.HeaderForeColor = System.Drawing.Color.Black;
            this.DataGrid.Location = new System.Drawing.Point(13, 343);
            this.DataGrid.Name = "DataGrid";
            this.DataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DataGrid.Size = new System.Drawing.Size(751, 197);
            this.DataGrid.TabIndex = 24;
            this.DataGrid.Click += new System.EventHandler(this.DataGrid_Click);
            // 
            // DdIn
            // 
            this.DdIn.ActiveBorderThickness = 1;
            this.DdIn.ActiveCornerRadius = 20;
            this.DdIn.ActiveFillColor = System.Drawing.Color.Lime;
            this.DdIn.ActiveForecolor = System.Drawing.Color.Black;
            this.DdIn.ActiveLineColor = System.Drawing.Color.Lime;
            this.DdIn.BackColor = System.Drawing.SystemColors.Control;
            this.DdIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("DdIn.BackgroundImage")));
            this.DdIn.ButtonText = "Ввести";
            this.DdIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DdIn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DdIn.ForeColor = System.Drawing.Color.Black;
            this.DdIn.IdleBorderThickness = 1;
            this.DdIn.IdleCornerRadius = 20;
            this.DdIn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DdIn.IdleForecolor = System.Drawing.Color.Black;
            this.DdIn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DdIn.Location = new System.Drawing.Point(14, 262);
            this.DdIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DdIn.Name = "DdIn";
            this.DdIn.Size = new System.Drawing.Size(104, 40);
            this.DdIn.TabIndex = 22;
            this.DdIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DdIn.Click += new System.EventHandler(this.DdIn_Click);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.ActiveBorderThickness = 1;
            this.ButtonEdit.ActiveCornerRadius = 20;
            this.ButtonEdit.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonEdit.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonEdit.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonEdit.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonEdit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonEdit.BackgroundImage")));
            this.ButtonEdit.ButtonText = "Изменить запись";
            this.ButtonEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonEdit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonEdit.ForeColor = System.Drawing.Color.Black;
            this.ButtonEdit.IdleBorderThickness = 1;
            this.ButtonEdit.IdleCornerRadius = 20;
            this.ButtonEdit.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonEdit.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonEdit.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonEdit.Location = new System.Drawing.Point(126, 262);
            this.ButtonEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.Size = new System.Drawing.Size(179, 40);
            this.ButtonEdit.TabIndex = 21;
            this.ButtonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ThinButtonRel
            // 
            this.ThinButtonRel.ActiveBorderThickness = 1;
            this.ThinButtonRel.ActiveCornerRadius = 20;
            this.ThinButtonRel.ActiveFillColor = System.Drawing.Color.Lime;
            this.ThinButtonRel.ActiveForecolor = System.Drawing.Color.Black;
            this.ThinButtonRel.ActiveLineColor = System.Drawing.Color.Lime;
            this.ThinButtonRel.BackColor = System.Drawing.SystemColors.Control;
            this.ThinButtonRel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ThinButtonRel.BackgroundImage")));
            this.ThinButtonRel.ButtonText = "Обновить";
            this.ThinButtonRel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ThinButtonRel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ThinButtonRel.ForeColor = System.Drawing.Color.Black;
            this.ThinButtonRel.IdleBorderThickness = 1;
            this.ThinButtonRel.IdleCornerRadius = 20;
            this.ThinButtonRel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ThinButtonRel.IdleForecolor = System.Drawing.Color.Black;
            this.ThinButtonRel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ThinButtonRel.Location = new System.Drawing.Point(425, 262);
            this.ThinButtonRel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ThinButtonRel.Name = "ThinButtonRel";
            this.ThinButtonRel.Size = new System.Drawing.Size(104, 40);
            this.ThinButtonRel.TabIndex = 20;
            this.ThinButtonRel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ButtonDel
            // 
            this.ButtonDel.ActiveBorderThickness = 1;
            this.ButtonDel.ActiveCornerRadius = 20;
            this.ButtonDel.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonDel.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonDel.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonDel.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonDel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonDel.BackgroundImage")));
            this.ButtonDel.ButtonText = "Удалить";
            this.ButtonDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDel.ForeColor = System.Drawing.Color.Black;
            this.ButtonDel.IdleBorderThickness = 1;
            this.ButtonDel.IdleCornerRadius = 20;
            this.ButtonDel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonDel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonDel.Location = new System.Drawing.Point(313, 262);
            this.ButtonDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonDel.Name = "ButtonDel";
            this.ButtonDel.Size = new System.Drawing.Size(104, 40);
            this.ButtonDel.TabIndex = 23;
            this.ButtonDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Закрыть";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(659, 548);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 19;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // RecordFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 592);
            this.Controls.Add(this.Ai92ProgressBar);
            this.Controls.Add(this.SmtProgressBar);
            this.Controls.Add(this.DtProgressBar);
            this.Controls.Add(this.Ai80ProgressBar);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ViewFuel);
            this.Controls.Add(this.LabelAi80);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelAi92);
            this.Controls.Add(this.ViewCounterparties);
            this.Controls.Add(this.LabelDt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelSmt);
            this.Controls.Add(this.LitersBox);
            this.Controls.Add(this.DataGrid);
            this.Controls.Add(this.DdIn);
            this.Controls.Add(this.ButtonEdit);
            this.Controls.Add(this.ThinButtonRel);
            this.Controls.Add(this.ButtonDel);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RecordFuel";
            this.Text = "Учёт";
            this.Load += new System.EventHandler(this.Records_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.MenuStripBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGauge Ai92ProgressBar;
        private Bunifu.Framework.UI.BunifuGauge SmtProgressBar;
        private Bunifu.Framework.UI.BunifuGauge DtProgressBar;
        private Bunifu.Framework.UI.BunifuGauge Ai80ProgressBar;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Coming;
        private System.Windows.Forms.RadioButton Consumption;
        private System.Windows.Forms.ContextMenuStrip MenuStripBar;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ViewFuel;
        private System.Windows.Forms.Label LabelAi80;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelAi92;
        private System.Windows.Forms.ComboBox ViewCounterparties;
        private System.Windows.Forms.Label LabelDt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelSmt;
        private System.Windows.Forms.TextBox LitersBox;
        private Bunifu.Framework.UI.BunifuCustomDataGrid DataGrid;
        private Bunifu.Framework.UI.BunifuThinButton2 DdIn;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonEdit;
        private Bunifu.Framework.UI.BunifuThinButton2 ThinButtonRel;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonDel;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
    }
}