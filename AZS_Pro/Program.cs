﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace AZS
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (Process.GetProcessesByName(Application.ProductName).Length > 1)
            {
                MessageBox.Show("Приложение уже запущено");
                return;
            }
            else
            {
                if (new ClassAll.Settings.Settings().ExistenceCheck()  == false)
                {
                    MessageBox.Show("Не найден Settings.cfg", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new GeneralForm());
                }
            }
        }
    }
}
