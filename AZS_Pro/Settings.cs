﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClassAll.Settings;
using Bunifu;
using BunifuAnimatorNS;

namespace AZS
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_MouseEnter(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            MeaningsSetting setting = new MeaningsSetting();
            TextboxSettingConnect.Text = setting.Connect();
        }

        private void Settings_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void Filediage_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            TextboxSettingConnect.Text = openFileDialog1.FileName;
        }
    }
}
