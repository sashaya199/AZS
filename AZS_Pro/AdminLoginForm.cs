﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Settings;
using System.Windows.Forms;

namespace AZS
{
    public partial class AdminLoginForm : Form
    {
        public AdminLoginForm()
        {
            InitializeComponent();
        }

        private void ButtonIn_Click(object sender, EventArgs e)
        {
            MeaningsSetting setting = new MeaningsSetting();
            if (setting.AdminCheck(LoginAdminTextBox.Text, PasswordAdminTextBox.Text)==true)
            {
                this.Dispose();
                new AddUsers().ShowDialog();
            }
            else
            {
                MessageBox.Show("Введён не правельный логин или пароль", "", MessageBoxButtons.OK);
                PasswordAdminTextBox.Text = "";
            }
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
