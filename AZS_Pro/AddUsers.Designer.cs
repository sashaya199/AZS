﻿namespace AZS
{
    partial class AddUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label имяLabel;
            System.Windows.Forms.Label фамилияLabel;
            System.Windows.Forms.Label отчествоLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddUsers));
            this.dataBaseDataSet = new AZS.DataBaseDataSet();
            this.authorizationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.authorizationTableAdapter = new AZS.DataBaseDataSetTableAdapters.AuthorizationTableAdapter();
            this.tableAdapterManager = new AZS.DataBaseDataSetTableAdapters.TableAdapterManager();
            this.authorizationBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.authorizationBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.ButtonOut = new System.Windows.Forms.ToolStripButton();
            this.имяTextBox = new System.Windows.Forms.TextBox();
            this.фамилияTextBox = new System.Windows.Forms.TextBox();
            this.отчествоTextBox = new System.Windows.Forms.TextBox();
            this.ButtonAssing = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Out = new Bunifu.Framework.UI.BunifuThinButton2();
            имяLabel = new System.Windows.Forms.Label();
            фамилияLabel = new System.Windows.Forms.Label();
            отчествоLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingNavigator)).BeginInit();
            this.authorizationBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // имяLabel
            // 
            имяLabel.AutoSize = true;
            имяLabel.Location = new System.Drawing.Point(26, 51);
            имяLabel.Name = "имяLabel";
            имяLabel.Size = new System.Drawing.Size(44, 19);
            имяLabel.TabIndex = 3;
            имяLabel.Text = "Имя:";
            // 
            // фамилияLabel
            // 
            фамилияLabel.AutoSize = true;
            фамилияLabel.Location = new System.Drawing.Point(26, 84);
            фамилияLabel.Name = "фамилияLabel";
            фамилияLabel.Size = new System.Drawing.Size(80, 19);
            фамилияLabel.TabIndex = 5;
            фамилияLabel.Text = "Фамилия:";
            // 
            // отчествоLabel
            // 
            отчествоLabel.AutoSize = true;
            отчествоLabel.Location = new System.Drawing.Point(26, 117);
            отчествоLabel.Name = "отчествоLabel";
            отчествоLabel.Size = new System.Drawing.Size(84, 19);
            отчествоLabel.TabIndex = 7;
            отчествоLabel.Text = "Отчество:";
            // 
            // dataBaseDataSet
            // 
            this.dataBaseDataSet.DataSetName = "DataBaseDataSet";
            this.dataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // authorizationBindingSource
            // 
            this.authorizationBindingSource.DataMember = "Authorization";
            this.authorizationBindingSource.DataSource = this.dataBaseDataSet;
            // 
            // authorizationTableAdapter
            // 
            this.authorizationTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AuthorizationTableAdapter = this.authorizationTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CounterpartiesTableAdapter = null;
            this.tableAdapterManager.FuelTableAdapter = null;
            this.tableAdapterManager.InventoriTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = AZS.DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // authorizationBindingNavigator
            // 
            this.authorizationBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.authorizationBindingNavigator.BindingSource = this.authorizationBindingSource;
            this.authorizationBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.authorizationBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.authorizationBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.authorizationBindingNavigatorSaveItem,
            this.ButtonOut});
            this.authorizationBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.authorizationBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.authorizationBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.authorizationBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.authorizationBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.authorizationBindingNavigator.Name = "authorizationBindingNavigator";
            this.authorizationBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.authorizationBindingNavigator.Size = new System.Drawing.Size(388, 25);
            this.authorizationBindingNavigator.TabIndex = 0;
            this.authorizationBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // authorizationBindingNavigatorSaveItem
            // 
            this.authorizationBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.authorizationBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("authorizationBindingNavigatorSaveItem.Image")));
            this.authorizationBindingNavigatorSaveItem.Name = "authorizationBindingNavigatorSaveItem";
            this.authorizationBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.authorizationBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.authorizationBindingNavigatorSaveItem.Click += new System.EventHandler(this.authorizationBindingNavigatorSaveItem_Click_1);
            // 
            // ButtonOut
            // 
            this.ButtonOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonOut.Image = global::AZS.Properties.Resources.close;
            this.ButtonOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(23, 22);
            this.ButtonOut.Text = "Выход";
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // имяTextBox
            // 
            this.имяTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.authorizationBindingSource, "Имя", true));
            this.имяTextBox.Location = new System.Drawing.Point(116, 48);
            this.имяTextBox.Name = "имяTextBox";
            this.имяTextBox.Size = new System.Drawing.Size(100, 27);
            this.имяTextBox.TabIndex = 4;
            // 
            // фамилияTextBox
            // 
            this.фамилияTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.authorizationBindingSource, "Фамилия", true));
            this.фамилияTextBox.Location = new System.Drawing.Point(116, 81);
            this.фамилияTextBox.Name = "фамилияTextBox";
            this.фамилияTextBox.Size = new System.Drawing.Size(100, 27);
            this.фамилияTextBox.TabIndex = 6;
            // 
            // отчествоTextBox
            // 
            this.отчествоTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.authorizationBindingSource, "Отчество", true));
            this.отчествоTextBox.Location = new System.Drawing.Point(116, 114);
            this.отчествоTextBox.Name = "отчествоTextBox";
            this.отчествоTextBox.Size = new System.Drawing.Size(100, 27);
            this.отчествоTextBox.TabIndex = 8;
            // 
            // ButtonAssing
            // 
            this.ButtonAssing.ActiveBorderThickness = 1;
            this.ButtonAssing.ActiveCornerRadius = 20;
            this.ButtonAssing.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonAssing.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonAssing.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonAssing.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonAssing.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonAssing.BackgroundImage")));
            this.ButtonAssing.ButtonText = "Назначить логин и пароль";
            this.ButtonAssing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAssing.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonAssing.ForeColor = System.Drawing.Color.SeaGreen;
            this.ButtonAssing.IdleBorderThickness = 1;
            this.ButtonAssing.IdleCornerRadius = 20;
            this.ButtonAssing.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAssing.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonAssing.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAssing.Location = new System.Drawing.Point(223, 39);
            this.ButtonAssing.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonAssing.Name = "ButtonAssing";
            this.ButtonAssing.Size = new System.Drawing.Size(163, 55);
            this.ButtonAssing.TabIndex = 9;
            this.ButtonAssing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonAssing.Click += new System.EventHandler(this.ButtonAssing_Click);
            // 
            // Out
            // 
            this.Out.ActiveBorderThickness = 1;
            this.Out.ActiveCornerRadius = 20;
            this.Out.ActiveFillColor = System.Drawing.Color.Lime;
            this.Out.ActiveForecolor = System.Drawing.Color.Black;
            this.Out.ActiveLineColor = System.Drawing.Color.Lime;
            this.Out.BackColor = System.Drawing.SystemColors.Control;
            this.Out.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Out.BackgroundImage")));
            this.Out.ButtonText = "Выход";
            this.Out.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Out.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Out.ForeColor = System.Drawing.Color.SeaGreen;
            this.Out.IdleBorderThickness = 1;
            this.Out.IdleCornerRadius = 20;
            this.Out.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Out.IdleForecolor = System.Drawing.Color.Black;
            this.Out.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Out.Location = new System.Drawing.Point(223, 86);
            this.Out.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Out.Name = "Out";
            this.Out.Size = new System.Drawing.Size(163, 55);
            this.Out.TabIndex = 9;
            this.Out.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Out.Click += new System.EventHandler(this.Out_Click);
            // 
            // AddUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 155);
            this.ControlBox = false;
            this.Controls.Add(this.Out);
            this.Controls.Add(this.ButtonAssing);
            this.Controls.Add(имяLabel);
            this.Controls.Add(this.имяTextBox);
            this.Controls.Add(фамилияLabel);
            this.Controls.Add(this.фамилияTextBox);
            this.Controls.Add(отчествоLabel);
            this.Controls.Add(this.отчествоTextBox);
            this.Controls.Add(this.authorizationBindingNavigator);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddUsers";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пользователи";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddUsers_FormClosing);
            this.Load += new System.EventHandler(this.AddUsers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingNavigator)).EndInit();
            this.authorizationBindingNavigator.ResumeLayout(false);
            this.authorizationBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataBaseDataSet dataBaseDataSet;
        private System.Windows.Forms.BindingSource authorizationBindingSource;
        private DataBaseDataSetTableAdapters.AuthorizationTableAdapter authorizationTableAdapter;
        private DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator authorizationBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton authorizationBindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton ButtonOut;
        private System.Windows.Forms.TextBox имяTextBox;
        private System.Windows.Forms.TextBox фамилияTextBox;
        private System.Windows.Forms.TextBox отчествоTextBox;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonAssing;
        private Bunifu.Framework.UI.BunifuThinButton2 Out;
    }
}