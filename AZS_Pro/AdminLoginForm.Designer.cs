﻿namespace AZS
{
    partial class AdminLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminLoginForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginAdminTextBox = new System.Windows.Forms.TextBox();
            this.PasswordAdminTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ButtonOut = new System.Windows.Forms.Button();
            this.ButtonIn = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Логин администратора";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Пароль";
            // 
            // LoginAdminTextBox
            // 
            this.LoginAdminTextBox.Location = new System.Drawing.Point(3, 22);
            this.LoginAdminTextBox.Name = "LoginAdminTextBox";
            this.LoginAdminTextBox.Size = new System.Drawing.Size(197, 27);
            this.LoginAdminTextBox.TabIndex = 4;
            // 
            // PasswordAdminTextBox
            // 
            this.PasswordAdminTextBox.Location = new System.Drawing.Point(3, 74);
            this.PasswordAdminTextBox.Name = "PasswordAdminTextBox";
            this.PasswordAdminTextBox.PasswordChar = '*';
            this.PasswordAdminTextBox.Size = new System.Drawing.Size(197, 27);
            this.PasswordAdminTextBox.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.LoginAdminTextBox);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.PasswordAdminTextBox);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(205, 120);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // ButtonOut
            // 
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.FlatAppearance.BorderSize = 0;
            this.ButtonOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonOut.Image = ((System.Drawing.Image)(resources.GetObject("ButtonOut.Image")));
            this.ButtonOut.Location = new System.Drawing.Point(131, 138);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(86, 31);
            this.ButtonOut.TabIndex = 1;
            this.ButtonOut.Text = "&Выход";
            this.ButtonOut.UseVisualStyleBackColor = true;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ButtonIn
            // 
            this.ButtonIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonIn.FlatAppearance.BorderSize = 0;
            this.ButtonIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonIn.Image = ((System.Drawing.Image)(resources.GetObject("ButtonIn.Image")));
            this.ButtonIn.Location = new System.Drawing.Point(12, 138);
            this.ButtonIn.Name = "ButtonIn";
            this.ButtonIn.Size = new System.Drawing.Size(86, 31);
            this.ButtonIn.TabIndex = 0;
            this.ButtonIn.Text = "Войти";
            this.ButtonIn.UseVisualStyleBackColor = true;
            this.ButtonIn.Click += new System.EventHandler(this.ButtonIn_Click);
            // 
            // AdminLoginForm
            // 
            this.AcceptButton = this.ButtonIn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(229, 181);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ButtonOut);
            this.Controls.Add(this.ButtonIn);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AdminLoginForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminLoginForm";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonIn;
        private System.Windows.Forms.Button ButtonOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LoginAdminTextBox;
        private System.Windows.Forms.TextBox PasswordAdminTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}