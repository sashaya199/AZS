﻿using System;
using ClassAll.Queries;
using ClassAll.Settings;
using System.Windows.Forms;

namespace AZS
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            if (LoginLabel.Text == "Логин")
            {
                if (new TestInquiry().Authorization(LoginTextBox.Text,PasswordTextBox.Text)==true)
                {
                    this.Dispose();
                }
            }
            else
            {
                if (new MeaningsSetting().AdminCheck(LoginTextBox.Text, PasswordTextBox.Text)== true)
                {
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Не правнльный логин или пароль", "Ошибка", MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }

        }

        private void ОПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void ВыходToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();

        private void ДобавитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LoginLabel.Text == "Логин")
            {
                MessageBox.Show("Нужны права администратора", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (new MeaningsSetting().AdminCheck(LoginTextBox.Text, PasswordTextBox.Text) == true)
                {
                    new AddUsers().ShowDialog();
                }
                else
                {
                    MessageBox.Show("Неправельно введён пароль или логин", "", MessageBoxButtons.OK);
                    PasswordTextBox.Text = "";
                }

            }
        }

        private void ВойтиПодАдминистраторомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LoginLabel.Text == "Логин")
            {
                LoginLabel.Text = "Логин администратора";
                войтиПодАдминистраторомToolStripMenuItem.Text = "Войти под пользователем";
            }
            else
            {
                LoginLabel.Text = "Логин";
                войтиПодАдминистраторомToolStripMenuItem.Text = "Войти под администратором";
                LoginTextBox.Text = "";
                PasswordTextBox.Text = "";
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            ToolTip.SetToolTip(LoginTextBox, "Введите логин");
            ToolTip.SetToolTip(PasswordTextBox, "Введите пароль");
            ToolTip.Active = true;
            ToolTip.IsBalloon = true;
        }
    }
}
