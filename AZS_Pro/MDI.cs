﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AZS
{
    class MDI
    {
        public void ShowMdiChild<TForm>(Form mdiParent, string parameter)
        where TForm : Form, new()
        {
            TForm mdiChild = mdiParent.MdiChildren.OfType<TForm>().SingleOrDefault();
            string mdi = mdiParent.Text;
            mdi = mdi.Substring(mdi.Length - 3);
            mdi = mdi.Remove(2, 1);
            if (parameter != mdi)
            {
                if (mdiChild != null)
                {
                    if (MessageBox.Show("Вы действительно желаете закрыть " + mdiChild.Text ,"",MessageBoxButtons.YesNo)==DialogResult.Yes)
                    {
                        mdiChild.Dispose();
                        mdiChild = CallForm<TForm>(mdiParent, parameter);
                    }
                }
                else
                {
                    mdiChild = CallForm<TForm>(mdiParent, parameter);
                }
            }
            else
            {
                mdiParent.Activate();
            }
        }

        private static TForm CallForm<TForm>(Form mdiParent, string parameter) where TForm : Form, new()
        {
            TForm mdiChild = new TForm
            {
                MdiParent = mdiParent,
                WindowState = FormWindowState.Maximized,
                Text = parameter,
            };
            mdiChild.Show();
            return mdiChild;
        }
    }

}
