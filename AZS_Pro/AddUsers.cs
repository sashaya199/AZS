﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AZS
{
    public partial class AddUsers : Form
    {
        public AddUsers()
        {
            InitializeComponent();
        }

        private void AuthorizationBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.authorizationBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataBaseDataSet);

        }

        private void AddUsers_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseDataSet.Authorization". При необходимости она может быть перемещена или удалена.
            this.authorizationTableAdapter.Fill(this.dataBaseDataSet.Authorization);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseDataSet.Authorization". При необходимости она может быть перемещена или удалена.
            this.authorizationTableAdapter.Fill(this.dataBaseDataSet.Authorization);

        }

        private void ClosseButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void AddUsers_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void authorizationBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.authorizationBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataBaseDataSet);

        }

        private void ButtonOut_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void ButtonAssing_Click(object sender, EventArgs e) => new AssingLodinAndPassword().ShowDialog();

        private void Out_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
