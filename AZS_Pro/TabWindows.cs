﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeifenLuo.WinFormsUI.Docking;
using System.Windows.Forms;

namespace AZS
{
    class TabWindows
    {
        public void OpenFormWindows<Dock>(DockPanel dockPanel, DockState dockState, string name)//с именем
        where Dock : DockContent, new()
        {
            Dock form = new Dock { };
            form.Text = $"{form.Text}  {name}";
            CallForms(dockPanel, form, dockState);
        }//
        
        public void OpenFormWindows<Dock>(DockPanel dockPanel, DockState dockState)//без именем
              where Dock : DockContent, new()
        {
            Dock form = new Dock { };
            CallForms(dockPanel, form, dockState);
        }//

        private static void CallForms<Dock>(DockPanel dockPanel, Dock form, DockState dockState) 
        where Dock : DockContent, new()
        {
            var coun = dockPanel.Contents.Count;//подсчёт открытых форм
            int count = 0;// счёчик на совпадения
            for (int i = 0; i <= coun - 1; i++)
            {
                if (dockPanel.Contents[i].DockHandler.TabText == form.Text)// если совпало то активируем форму
                {
                    count++;
                    dockPanel.Contents[i].DockHandler.Activate();
                    break;
                }
            }
            if (count == 0)// если совпадений нет то вызываем форму
            {
                form.Show(dockPanel, dockState);
            }
        }
    }
}
