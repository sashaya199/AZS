﻿namespace AZS
{
    partial class GeneralForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.фаилToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.учётToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрагентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьПоставщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.топливоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьТопливоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ДобавитьОператораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвентаризацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчётToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвентаризацияToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отчётToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.поТопливуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.реквизитыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.иАToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сМТToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дТToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.фаилToolStripMenuItem,
            this.инвентаризацияToolStripMenuItem,
            this.отчётToolStripMenuItem,
            this.справкаToolStripMenuItem,
            this.настройкиToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 26);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // фаилToolStripMenuItem
            // 
            this.фаилToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.учётToolStripMenuItem,
            this.контрагентыToolStripMenuItem,
            this.добавитьПоставщикаToolStripMenuItem,
            this.топливоToolStripMenuItem,
            this.ДобавитьОператораToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.фаилToolStripMenuItem.Name = "фаилToolStripMenuItem";
            this.фаилToolStripMenuItem.Size = new System.Drawing.Size(76, 22);
            this.фаилToolStripMenuItem.Text = "Главная";
            // 
            // учётToolStripMenuItem
            // 
            this.учётToolStripMenuItem.Image = global::AZS.Properties.Resources.accounting;
            this.учётToolStripMenuItem.Name = "учётToolStripMenuItem";
            this.учётToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.учётToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.учётToolStripMenuItem.Text = "Учёт";
            this.учётToolStripMenuItem.Click += new System.EventHandler(this.УчётToolStripMenuItem_Click);
            // 
            // контрагентыToolStripMenuItem
            // 
            this.контрагентыToolStripMenuItem.Image = global::AZS.Properties.Resources.Human1;
            this.контрагентыToolStripMenuItem.Name = "контрагентыToolStripMenuItem";
            this.контрагентыToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.контрагентыToolStripMenuItem.Text = "Контрагенты";
            this.контрагентыToolStripMenuItem.Click += new System.EventHandler(this.КонтрагентыToolStripMenuItem_Click);
            // 
            // добавитьПоставщикаToolStripMenuItem
            // 
            this.добавитьПоставщикаToolStripMenuItem.Image = global::AZS.Properties.Resources.Human1;
            this.добавитьПоставщикаToolStripMenuItem.Name = "добавитьПоставщикаToolStripMenuItem";
            this.добавитьПоставщикаToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.добавитьПоставщикаToolStripMenuItem.Text = "Поставщики";
            this.добавитьПоставщикаToolStripMenuItem.Click += new System.EventHandler(this.добавитьПоставщикаToolStripMenuItem_Click);
            // 
            // топливоToolStripMenuItem
            // 
            this.топливоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьТопливоToolStripMenuItem,
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem,
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem});
            this.топливоToolStripMenuItem.Image = global::AZS.Properties.Resources.Fuel;
            this.топливоToolStripMenuItem.Name = "топливоToolStripMenuItem";
            this.топливоToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.топливоToolStripMenuItem.Text = "Топливо";
            // 
            // добавитьТопливоToolStripMenuItem
            // 
            this.добавитьТопливоToolStripMenuItem.Name = "добавитьТопливоToolStripMenuItem";
            this.добавитьТопливоToolStripMenuItem.Size = new System.Drawing.Size(380, 22);
            this.добавитьТопливоToolStripMenuItem.Text = "Добавить топливо";
            this.добавитьТопливоToolStripMenuItem.Click += new System.EventHandler(this.ДобавитьВидТопливаToolStripMenuItem_Click);
            // 
            // заполнитьГрадуировочнуюТаблицуToolStripMenuItem
            // 
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem.Name = "заполнитьГрадуировочнуюТаблицуToolStripMenuItem";
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem.Size = new System.Drawing.Size(380, 22);
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem.Text = "Заполнить градуировочную таблицу";
            this.заполнитьГрадуировочнуюТаблицуToolStripMenuItem.Click += new System.EventHandler(this.ЗаполнитьToolStripMenuItem_Click);
            // 
            // просмотТопливаИГрадуировочныхТаблицToolStripMenuItem
            // 
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem.Name = "просмотТопливаИГрадуировочныхТаблицToolStripMenuItem";
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem.Size = new System.Drawing.Size(380, 22);
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem.Text = "Просмот топлива и градуировочных таблиц";
            this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem.Click += new System.EventHandler(this.просмотТопливаИГрадуировочныхТаблицToolStripMenuItem_Click);
            // 
            // ДобавитьОператораToolStripMenuItem
            // 
            this.ДобавитьОператораToolStripMenuItem.Image = global::AZS.Properties.Resources.HumanAdd;
            this.ДобавитьОператораToolStripMenuItem.Name = "ДобавитьОператораToolStripMenuItem";
            this.ДобавитьОператораToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.ДобавитьОператораToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.ДобавитьОператораToolStripMenuItem.Text = "Добавить оператора";
            this.ДобавитьОператораToolStripMenuItem.Visible = false;
            this.ДобавитьОператораToolStripMenuItem.Click += new System.EventHandler(this.ДобавитьОператораToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::AZS.Properties.Resources.close;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.ВыходToolStripMenuItem_Click);
            // 
            // инвентаризацияToolStripMenuItem
            // 
            this.инвентаризацияToolStripMenuItem.Name = "инвентаризацияToolStripMenuItem";
            this.инвентаризацияToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.инвентаризацияToolStripMenuItem.Text = "Инвентаризация";
            // 
            // отчётToolStripMenuItem
            // 
            this.отчётToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.инвентаризацияToolStripMenuItem1,
            this.отчётToolStripMenuItem1,
            this.поТопливуToolStripMenuItem});
            this.отчётToolStripMenuItem.Name = "отчётToolStripMenuItem";
            this.отчётToolStripMenuItem.Size = new System.Drawing.Size(63, 22);
            this.отчётToolStripMenuItem.Text = "Отчёт";
            // 
            // инвентаризацияToolStripMenuItem1
            // 
            this.инвентаризацияToolStripMenuItem1.Name = "инвентаризацияToolStripMenuItem1";
            this.инвентаризацияToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.инвентаризацияToolStripMenuItem1.Text = "Инвентаризация";
            this.инвентаризацияToolStripMenuItem1.Click += new System.EventHandler(this.ИнвентаризацияToolStripMenuItem1_Click);
            // 
            // отчётToolStripMenuItem1
            // 
            this.отчётToolStripMenuItem1.Name = "отчётToolStripMenuItem1";
            this.отчётToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.отчётToolStripMenuItem1.Text = "По контрагентам";
            this.отчётToolStripMenuItem1.Click += new System.EventHandler(this.ОтчётToolStripMenuItem1_Click);
            // 
            // поТопливуToolStripMenuItem
            // 
            this.поТопливуToolStripMenuItem.Name = "поТопливуToolStripMenuItem";
            this.поТопливуToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.поТопливуToolStripMenuItem.Text = "По топливу";
            this.поТопливуToolStripMenuItem.Click += new System.EventHandler(this.ПоТопливуToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.реквизитыToolStripMenuItem,
            this.оПрограммеToolStripMenuItem,
            this.помощьToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(76, 22);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // реквизитыToolStripMenuItem
            // 
            this.реквизитыToolStripMenuItem.Name = "реквизитыToolStripMenuItem";
            this.реквизитыToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.реквизитыToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.реквизитыToolStripMenuItem.Text = "Реквизиты";
            this.реквизитыToolStripMenuItem.Visible = false;
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.ОПрограммеToolStripMenuItem_Click);
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.помощьToolStripMenuItem.Text = "Помощь";
            this.помощьToolStripMenuItem.Visible = false;
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Visible = false;
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.НастройкиToolStripMenuItem_Click);
            // 
            // иАToolStripMenuItem
            // 
            this.иАToolStripMenuItem.Name = "иАToolStripMenuItem";
            this.иАToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.иАToolStripMenuItem.Text = "ИА";
            // 
            // сМТToolStripMenuItem
            // 
            this.сМТToolStripMenuItem.Name = "сМТToolStripMenuItem";
            this.сМТToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.сМТToolStripMenuItem.Text = "СМТ";
            // 
            // дТToolStripMenuItem
            // 
            this.дТToolStripMenuItem.Name = "дТToolStripMenuItem";
            this.дТToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.дТToolStripMenuItem.Text = "ДТ";
            // 
            // DockPanel
            // 
            this.DockPanel.ActiveAutoHideContent = null;
            this.DockPanel.AutoScroll = true;
            this.DockPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DockPanel.DockBackColor = System.Drawing.SystemColors.Control;
            this.DockPanel.Location = new System.Drawing.Point(0, 26);
            this.DockPanel.Name = "DockPanel";
            this.DockPanel.Size = new System.Drawing.Size(934, 536);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient1.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient1.TextColor = System.Drawing.Color.Black;
            autoHideStripSkin1.TabGradient = tabGradient1;
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.Color.Black;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.Color.Black;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.DockPanel.Skin = dockPanelSkin1;
            this.DockPanel.TabIndex = 7;
            // 
            // GeneralForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 562);
            this.Controls.Add(this.DockPanel);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(945, 600);
            this.Name = "GeneralForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АЗС";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GeneralForm_FormClosing);
            this.Load += new System.EventHandler(this.GeneralForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem фаилToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ДобавитьОператораToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem инвентаризацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчётToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инвентаризацияToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem отчётToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem иАToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сМТToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дТToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem реквизитыToolStripMenuItem;
        private WeifenLuo.WinFormsUI.Docking.DockPanel DockPanel;
        private System.Windows.Forms.ToolStripMenuItem учётToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьПоставщикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поТопливуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрагентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem топливоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьТопливоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заполнитьГрадуировочнуюТаблицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem просмотТопливаИГрадуировочныхТаблицToolStripMenuItem;
    }
}

