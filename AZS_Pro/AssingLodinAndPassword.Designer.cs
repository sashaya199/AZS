﻿namespace AZS
{
    partial class AssingLodinAndPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssingLodinAndPassword));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelLogin = new System.Windows.Forms.Label();
            this.TextBoxLogin = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.TextBoxPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ComboBoxUser = new System.Windows.Forms.ComboBox();
            this.ButtonAssing = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Out = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dataBaseDataSet = new AZS.DataBaseDataSet();
            this.authorizationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.authorizationTableAdapter = new AZS.DataBaseDataSetTableAdapters.AuthorizationTableAdapter();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.labelLogin);
            this.flowLayoutPanel1.Controls.Add(this.TextBoxLogin);
            this.flowLayoutPanel1.Controls.Add(this.labelPassword);
            this.flowLayoutPanel1.Controls.Add(this.TextBoxPassword);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(229, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(207, 109);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(3, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(60, 19);
            this.labelLogin.TabIndex = 1;
            this.labelLogin.Text = "Логин:";
            // 
            // TextBoxLogin
            // 
            this.TextBoxLogin.Location = new System.Drawing.Point(3, 22);
            this.TextBoxLogin.Name = "TextBoxLogin";
            this.TextBoxLogin.Size = new System.Drawing.Size(199, 27);
            this.TextBoxLogin.TabIndex = 5;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(3, 52);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(69, 19);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Пароль:";
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Location = new System.Drawing.Point(3, 74);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.Size = new System.Drawing.Size(197, 27);
            this.TextBoxPassword.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "Пользователь:";
            // 
            // ComboBoxUser
            // 
            this.ComboBoxUser.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.authorizationBindingSource, "Фамилия", true));
            this.ComboBoxUser.DataSource = this.authorizationBindingSource;
            this.ComboBoxUser.DisplayMember = "Фамилия";
            this.ComboBoxUser.FormattingEnabled = true;
            this.ComboBoxUser.Location = new System.Drawing.Point(13, 41);
            this.ComboBoxUser.Name = "ComboBoxUser";
            this.ComboBoxUser.Size = new System.Drawing.Size(209, 27);
            this.ComboBoxUser.TabIndex = 4;
            this.ComboBoxUser.ValueMember = "Фамилия";
            // 
            // ButtonAssing
            // 
            this.ButtonAssing.ActiveBorderThickness = 1;
            this.ButtonAssing.ActiveCornerRadius = 20;
            this.ButtonAssing.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonAssing.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonAssing.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonAssing.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonAssing.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonAssing.BackgroundImage")));
            this.ButtonAssing.ButtonText = "Назначить";
            this.ButtonAssing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAssing.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonAssing.ForeColor = System.Drawing.Color.SeaGreen;
            this.ButtonAssing.IdleBorderThickness = 1;
            this.ButtonAssing.IdleCornerRadius = 20;
            this.ButtonAssing.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAssing.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonAssing.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAssing.Location = new System.Drawing.Point(13, 81);
            this.ButtonAssing.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonAssing.Name = "ButtonAssing";
            this.ButtonAssing.Size = new System.Drawing.Size(103, 50);
            this.ButtonAssing.TabIndex = 5;
            this.ButtonAssing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonAssing.Click += new System.EventHandler(this.ButtonAssing_Click);
            // 
            // Out
            // 
            this.Out.ActiveBorderThickness = 1;
            this.Out.ActiveCornerRadius = 20;
            this.Out.ActiveFillColor = System.Drawing.Color.Lime;
            this.Out.ActiveForecolor = System.Drawing.Color.Black;
            this.Out.ActiveLineColor = System.Drawing.Color.Lime;
            this.Out.BackColor = System.Drawing.SystemColors.Control;
            this.Out.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Out.BackgroundImage")));
            this.Out.ButtonText = "&Выход";
            this.Out.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Out.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Out.ForeColor = System.Drawing.Color.SeaGreen;
            this.Out.IdleBorderThickness = 1;
            this.Out.IdleCornerRadius = 20;
            this.Out.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Out.IdleForecolor = System.Drawing.Color.Black;
            this.Out.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Out.Location = new System.Drawing.Point(119, 81);
            this.Out.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Out.Name = "Out";
            this.Out.Size = new System.Drawing.Size(103, 50);
            this.Out.TabIndex = 5;
            this.Out.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Out.Click += new System.EventHandler(this.Out_Click);
            // 
            // dataBaseDataSet
            // 
            this.dataBaseDataSet.DataSetName = "DataBaseDataSet";
            this.dataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // authorizationBindingSource
            // 
            this.authorizationBindingSource.DataMember = "Authorization";
            this.authorizationBindingSource.DataSource = this.dataBaseDataSet;
            // 
            // authorizationTableAdapter
            // 
            this.authorizationTableAdapter.ClearBeforeFill = true;
            // 
            // AssingLodinAndPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 140);
            this.ControlBox = false;
            this.Controls.Add(this.Out);
            this.Controls.Add(this.ButtonAssing);
            this.Controls.Add(this.ComboBoxUser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AssingLodinAndPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Логин и пароль";
            this.Load += new System.EventHandler(this.AssingLodinAndPassword_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorizationBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ComboBoxUser;
        private System.Windows.Forms.TextBox TextBoxLogin;
        private System.Windows.Forms.TextBox TextBoxPassword;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonAssing;
        private Bunifu.Framework.UI.BunifuThinButton2 Out;
        private DataBaseDataSet dataBaseDataSet;
        private System.Windows.Forms.BindingSource authorizationBindingSource;
        private DataBaseDataSetTableAdapters.AuthorizationTableAdapter authorizationTableAdapter;
    }
}