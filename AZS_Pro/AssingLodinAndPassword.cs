﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ClassAll.Queries;
using System.Windows.Forms;

namespace AZS
{
    public partial class AssingLodinAndPassword : Form
    {
        public AssingLodinAndPassword()
        {
            InitializeComponent();
        }

        private void AssingLodinAndPassword_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dataBaseDataSet.Authorization". При необходимости она может быть перемещена или удалена.
            this.authorizationTableAdapter.Fill(this.dataBaseDataSet.Authorization);
        }

        private void ButtonAssing_Click(object sender, EventArgs e)
        {
            using (RenevalInquiry reneval = new RenevalInquiry())
            {
                reneval.AddLoginAndPassword(TextBoxLogin.Text, TextBoxPassword.Text, ComboBoxUser.Text);
                MessageBox.Show($"{reneval.Msg}");
            }
        }

        private void Out_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
