﻿using System;
using Reports;
using Inventori;
using Record;
using Record.Calibration;
using ClassAll;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using ClassAll.Queries;

namespace AZS
{
    public partial class GeneralForm : Form
    {
        public GeneralForm()
        {
            InitializeComponent();
        }
        private TabWindows windows = new TabWindows();
        private OpenWindowForm openForm = new OpenWindowForm();
        private void GeneralForm_Load(object sender, EventArgs e)
        {
            new LoginForm().ShowDialog();
            UpdataMenu();
        }
        private void UpdataMenu()
        {
            string[] menuFuel = new InquiryMeaning().AllFuel();
            foreach (var item in menuFuel)
            {
                инвентаризацияToolStripMenuItem.DropDownItems.Add(item);
            }
            инвентаризацияToolStripMenuItem.DropDownItemClicked += ИнвентаризацияToolStripMenuItem_DropDownItemClicked;
        }

        private void ИнвентаризацияToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e) => windows.OpenFormWindows<InventoriVariable>(DockPanel, DockState.Document, e.ClickedItem.Text);

        private void GeneralForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете выйти?", "Выход", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ВыходToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();

        private void ДобавитьОператораToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Для добавления оператора нужны правда администратора. Продолжить?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
               new AdminLoginForm().ShowDialog();
            }
        }

        private void ОПрограммеToolStripMenuItem_Click(object sender, EventArgs e) => openForm.OpenForms<AboutBox>();

        private void ДобавитьВидТопливаToolStripMenuItem_Click(object sender, EventArgs e) => openForm.OpenForms<AddFuel>();

        private void УчётToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (TestInquiry inquiry = new TestInquiry())
            {
                if (inquiry.Test())
                {
                    windows.OpenFormWindows<RecordFuel>(DockPanel, DockState.Document);
                }
            }

        }

        private void ОтчётToolStripMenuItem1_Click(object sender, EventArgs e) => windows.OpenFormWindows<ReportRecordConsumption>(DockPanel, DockState.Document);

        private void НастройкиToolStripMenuItem_Click(object sender, EventArgs e) => openForm.OpenForms<Settings>();

        private void ИнвентаризацияToolStripMenuItem1_Click(object sender, EventArgs e) => windows.OpenFormWindows<ReportsInventori>(DockPanel, DockState.Document);

        private void ПоТопливуToolStripMenuItem_Click(object sender, EventArgs e) => windows.OpenFormWindows<ReportRecordFuel>(DockPanel, DockState.Document);

        private void КонтрагентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DockPanel.Contents.Count == 0)
            {
                new AddCounterpartiesProvider("Контрагенты").ShowDialog();
            }
            else
            {
                MessageBox.Show("Для изминения списка контрагентов необходимо закрыть все вкладки","",MessageBoxButtons.OK);
            }
        }

        private void добавитьПоставщикаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DockPanel.Contents.Count == 0)
            {
                new AddCounterpartiesProvider("Поставщики").ShowDialog();
            }
            else
            {
                MessageBox.Show("Для изминения списка контрагентов необходимо закрыть все вкладки", "", MessageBoxButtons.OK);
            }
        }

        private void ЗаполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DockPanel.Contents.Count == 0)
            {
                using (TestInquiry inquiry = new TestInquiry())
                {
                    if (inquiry.EmptyFuelTable().Length > 0)
                    {
                        new CalibrationValue().ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Все таблицы заполнены.");
                    }
                }
            }
            else
            {
                MessageBox.Show("Для изминения калибровки необходимо закрыть все вкладки", "", MessageBoxButtons.OK);
            }
        }

        private void просмотТопливаИГрадуировочныхТаблицToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FuelTableCalibration().Show();
        }
    }
}
