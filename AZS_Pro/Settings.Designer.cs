﻿namespace AZS
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.ButtonOut = new Bunifu.Framework.UI.BunifuThinButton2();
            this.ButtonAccpet = new Bunifu.Framework.UI.BunifuThinButton2();
            this.TextboxSettingConnect = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.ButtonFileDialoge = new Bunifu.Framework.UI.BunifuThinButton2();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // ButtonOut
            // 
            this.ButtonOut.ActiveBorderThickness = 1;
            this.ButtonOut.ActiveCornerRadius = 20;
            this.ButtonOut.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonOut.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonOut.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOut.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonOut.BackgroundImage")));
            this.ButtonOut.ButtonText = "Выход";
            this.ButtonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonOut.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonOut.ForeColor = System.Drawing.Color.Black;
            this.ButtonOut.IdleBorderThickness = 1;
            this.ButtonOut.IdleCornerRadius = 20;
            this.ButtonOut.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonOut.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonOut.Location = new System.Drawing.Point(513, 339);
            this.ButtonOut.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonOut.Name = "ButtonOut";
            this.ButtonOut.Size = new System.Drawing.Size(104, 40);
            this.ButtonOut.TabIndex = 0;
            this.ButtonOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonOut.Click += new System.EventHandler(this.ButtonOut_Click);
            // 
            // ButtonAccpet
            // 
            this.ButtonAccpet.ActiveBorderThickness = 1;
            this.ButtonAccpet.ActiveCornerRadius = 20;
            this.ButtonAccpet.ActiveFillColor = System.Drawing.Color.Lime;
            this.ButtonAccpet.ActiveForecolor = System.Drawing.Color.Black;
            this.ButtonAccpet.ActiveLineColor = System.Drawing.Color.Lime;
            this.ButtonAccpet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonAccpet.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonAccpet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonAccpet.BackgroundImage")));
            this.ButtonAccpet.ButtonText = "Приминить";
            this.ButtonAccpet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAccpet.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonAccpet.ForeColor = System.Drawing.Color.Black;
            this.ButtonAccpet.IdleBorderThickness = 1;
            this.ButtonAccpet.IdleCornerRadius = 20;
            this.ButtonAccpet.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAccpet.IdleForecolor = System.Drawing.Color.Black;
            this.ButtonAccpet.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ButtonAccpet.Location = new System.Drawing.Point(13, 339);
            this.ButtonAccpet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonAccpet.Name = "ButtonAccpet";
            this.ButtonAccpet.Size = new System.Drawing.Size(110, 40);
            this.ButtonAccpet.TabIndex = 1;
            this.ButtonAccpet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TextboxSettingConnect
            // 
            this.TextboxSettingConnect.BorderColorFocused = System.Drawing.Color.Blue;
            this.TextboxSettingConnect.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TextboxSettingConnect.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.TextboxSettingConnect.BorderThickness = 3;
            this.TextboxSettingConnect.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextboxSettingConnect.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextboxSettingConnect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TextboxSettingConnect.isPassword = false;
            this.TextboxSettingConnect.Location = new System.Drawing.Point(13, 53);
            this.TextboxSettingConnect.Margin = new System.Windows.Forms.Padding(4);
            this.TextboxSettingConnect.Name = "TextboxSettingConnect";
            this.TextboxSettingConnect.Size = new System.Drawing.Size(416, 49);
            this.TextboxSettingConnect.TabIndex = 2;
            this.TextboxSettingConnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // ButtonFileDialoge
            // 
            this.ButtonFileDialoge.ActiveBorderThickness = 1;
            this.ButtonFileDialoge.ActiveCornerRadius = 20;
            this.ButtonFileDialoge.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.ButtonFileDialoge.ActiveForecolor = System.Drawing.Color.White;
            this.ButtonFileDialoge.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.ButtonFileDialoge.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonFileDialoge.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonFileDialoge.BackgroundImage")));
            this.ButtonFileDialoge.ButtonText = "";
            this.ButtonFileDialoge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonFileDialoge.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonFileDialoge.ForeColor = System.Drawing.Color.SeaGreen;
            this.ButtonFileDialoge.IdleBorderThickness = 1;
            this.ButtonFileDialoge.IdleCornerRadius = 20;
            this.ButtonFileDialoge.IdleFillColor = System.Drawing.Color.White;
            this.ButtonFileDialoge.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.ButtonFileDialoge.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.ButtonFileDialoge.Location = new System.Drawing.Point(438, 53);
            this.ButtonFileDialoge.Margin = new System.Windows.Forms.Padding(5);
            this.ButtonFileDialoge.Name = "ButtonFileDialoge";
            this.ButtonFileDialoge.Size = new System.Drawing.Size(42, 41);
            this.ButtonFileDialoge.TabIndex = 3;
            this.ButtonFileDialoge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ButtonFileDialoge.Click += new System.EventHandler(this.Filediage_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "База (*.accdb)| *.accdb";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 393);
            this.Controls.Add(this.ButtonFileDialoge);
            this.Controls.Add(this.TextboxSettingConnect);
            this.Controls.Add(this.ButtonAccpet);
            this.Controls.Add(this.ButtonOut);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Settings_FormClosing);
            this.Load += new System.EventHandler(this.Settings_Load);
            this.MouseEnter += new System.EventHandler(this.Settings_MouseEnter);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 ButtonOut;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonAccpet;
        private Bunifu.Framework.UI.BunifuMetroTextbox TextboxSettingConnect;
        private Bunifu.Framework.UI.BunifuThinButton2 ButtonFileDialoge;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}